import itertools

def get_dict():
    return {"pageable to": {},
            "pinned from": {},
            "pageable from": {},
            "pinned to": {}}

my_dict = get_dict()

def grouplen(seq, n):
    return zip(*[iter(seq)]*n)

def get_significant(lines):
    lines = (line.split() for line in lines)
    line = next(lines)
    size = line[5]
    malloc_type = line[3]
    to_speed = next(lines)[3]
    from_speed = next(lines)[3]
    return (int(size), malloc_type, float(to_speed), float(from_speed))


with open("bw_timings.out") as fh:
    lines = grouplen(fh, 3)
    lines = (get_significant(x) for x in lines)
    for size, malloc_type, to_speed, from_speed in lines:
        my_dict["%s %s" % (malloc_type, "to")][size] = to_speed
        my_dict["%s %s" % (malloc_type, "from")][size] = from_speed


import json
print json.dump(my_dict, open("bw_timings.json","w"), indent=2, sort_keys=True)
