import json

def _get_number(line):
    return int(line[5])

def _get_async(line):
    return line[1]

def _get_type_and_time(line):
    #return line[-2].split("...")
    return [line[6][:-3], line[7]]

def process_file(name, outname):
    my_dict2 = {}
    with open(name) as file_:
        lines = [line.strip().split() for line in file_]
        #del lines[-1]

        split_lines = ((_get_number(line), _get_async(line),
                        _get_type_and_time(line)) for line in lines)

        for number, async, [type_, time] in split_lines:
            column = "%s %s" % (async[:async.find("hr")], type_)
            if column not in my_dict2:
                my_dict2[column] = {}
            my_dict2[column][number] = time

    json.dump(my_dict2, open(outname, "w"), indent=2, sort_keys=True)
