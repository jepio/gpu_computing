\documentclass[a4paper,12pt]{article}

\usepackage[T1]{fontenc}
\usepackage{titling}
\usepackage{indentfirst}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{color}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=C++,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true
}

\title{Excercise 2}
\author{Bernd Epding, Jeremi Piotrowski}
\setlength{\droptitle}{-10em}

\begin{document}

\maketitle \thispagestyle{empty}


%\lstinputlisting[language=C++]{jeremi/nullKernelAsync_jeremi_final.cu}

\section*{Exercise 2.1}

\begin{figure}[bh]
\centering
\includegraphics[width=0.8 \textwidth]{kernel_startup.pdf}
\caption{Comparison of kernel startup times with varying thread and block number - synchronous and asynchronous kernel launches}
\label{kernel_startup_times}
\end{figure}

\emph{Interpretation:}
Changing thread number from 1 to 1024 does not change sync or async launch time since always only one block is executed on the device that can execute enough warps in parallel. Varying the block number should change launch time, since only ?one block per core is executed at a time?. But a change is only visible from the point on where overhead of starting the kernel, ie. communication between CPU and GPU and data transfer is overcome and/or more blocks are executed than ?cuda cores? are available on the card. In our case the card should have 448 cuda cores. One can see a rise in time between the asynchronous calls with 384 and 512 blocks and from there on a linear increase in execution time as expected. Since the program always has to wait for one launch call to finish all blocks and return, synchronous calls generally take much longer than the pipelined asynchronous calls. The times for synchronous kernel launches with bigger block number grow almost linearly from the start. With growing block number there seems to be an additional overhead when the launch calls are not pipelined on the device (in the range of 128 to 384 blocks per launch). Probably due to more necessary communication between host and device. \\ 


\section*{Exercise 2.2}

The number of clock cycles one has to busy wait is around 4400. By then the asynchronous launch time doubles.


\section*{Exercise 2.3}

\begin{figure}[bh]
\centering
\includegraphics[width=0.8 \textwidth]{data_transfer.pdf}
\caption{Comparison of data transfer speeds when using pagable and pinned memory and varying data sizes}
\label{data_transfer_bandwith}
\end{figure}

\emph{Interpretation:}
It can be seen from figure 2 that at any transfer size pinned memory enables a higher bandwidth
than pageable memory. This is understandable, as pinned memory is present in RAM
at all times, does not require swapping and can be accessed by the GPU
directly. For small transfer sizes we see a very low transfer rate. This is
caused by the data transfer initiation overhead being a big part of the overall
transfer time under those conditions. We also see that for a higher transfer size the bandwidth grows until
saturation. Pageable memory transfers to device saturate around 2.5~GB/s while
pinned memory accesses saturate around 6~GB/s. These numbers are related to the PCIe bus speed of
around 8~GB/s. We were surprised to find that pageable memory transfers from the
device are sped up above 1 MB. We believe that the device performs some form of
pinning of memory to enable faster transfers back to the host.

\section*{Code}

\begin{lstlisting}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "chTimer.h"

// how many clocks the kernel took
__device__ clock_t d_Cycles;
// device variable that prevents the kernel from being optimized out
__device__ int d_i;


// Do-nothing kernel
__global__ void NullKernel() { }

// Busy wait kernel that increments a global counter
__global__ void BusyKernel(int iter)
{
    clock_t start_time = clock();

    // Wait for a certain number of iterations
    int i = 0;
    while (i < iter) {
        d_i = i++;
    }

    // Write the number of clock cycles to a device variable
    if (threadIdx.x == 0){
        d_Cycles = clock() - start_time;
    }

}

enum sync { synchronous, asynchronous};
enum vary { thread, block};

double measure(const int n, sync s, vary v, const int cIterations)
{

    dim3 threads(1, 1, 1);
    dim3 blocks(1, 1, 1);
    // messages for the printf
    const char *buff = (s == synchronous ? "synchronous" : "asynchronous");
    const char *buff2 = (v == thread ? "threads" : "blocks");

    // Vary the thing that is supposed to be varied
    if (v == thread){
        threads.x = n;
    } else {
        blocks.x = n;
    }

    printf("Measuring %s launch time for %u %s... ", buff, n, buff2);
    fflush(stdout);

    // Start the clock
    chTimerTimestamp start, stop;
    chTimerGetTime(&start);

    for (int i = 0; i < cIterations; ++i){
        // Invoke kernel
        NullKernel<<<blocks, threads>>>();
        if (s == synchronous){
            // synchronize after each kernel
            cudaDeviceSynchronize();
        }
    }
    if (s == asynchronous){
        // synchronize after all kernels
        cudaDeviceSynchronize();
    }

    // Stop the clock
    chTimerGetTime(&stop);
    double microseconds = 1e6 * chTimerElapsedTime(&start, &stop);
    double usPerLaunch = microseconds / cIterations;

    printf("%.2f us \n", usPerLaunch);

    return usPerLaunch;
}

void exercise01()
{
    const int cIterations = 500000;
    // Special case for 1 thread
    measure(1, synchronous, thread, cIterations);
    measure(1, synchronous, block, cIterations);
    measure(1, asynchronous, thread, cIterations);
    measure(1, asynchronous, block, cIterations);

    for (int i = 32; i <= 1024; i += 32 ){
        measure(i, synchronous, thread, cIterations);
        measure(i, synchronous, block, cIterations);
        measure(i, asynchronous, thread, cIterations);
        measure(i, asynchronous, block, cIterations);
    }
}

double launchBusyKernel(clock_t clocks, const int cIterations,
                        bool verbose=false)
{
    chTimerTimestamp start, stop;
    chTimerGetTime(&start);

    // Run the BusyKernel many times
    for (int i = 0; i < cIterations; ++i){
        BusyKernel<<<1, 1>>>(clocks);
    }
    cudaDeviceSynchronize();
    chTimerGetTime(&stop);

    double microseconds = 1e6 * chTimerElapsedTime(&start, &stop);
    double usPerLaunch = microseconds / cIterations;

    clock_t h_Cycles = 0;
    cudaMemcpyFromSymbol(&h_Cycles, d_Cycles, sizeof(clock_t),
                         0, cudaMemcpyDeviceToHost);

    if (verbose) {
        printf("%.2f us / %ld clocks \n", usPerLaunch, h_Cycles);
    }
    return usPerLaunch;
}

void exercise02()
{
    // Warmup
    BusyKernel<<<1, 1>>>(500);
    cudaDeviceSynchronize();

    const int cIterations = 500000;

    // Reference check for 0 iterations in kernel.
    printf("Reference check\n");
    printf("Running BusyKernel with  0 iterations... ");
    double reference_runtime = launchBusyKernel(0, cIterations, true);
    double runtime = 0.0;
    int iterations = 0;
    printf("\n");

    while (runtime < 2*reference_runtime){
        iterations += 4;
        printf("Running BusyKernel with %2d iterations... ", iterations);
        runtime = launchBusyKernel(iterations, cIterations, true);
    }
    printf("Kernel startup time has doubled.\n");
    printf("Done!\n");
}

template <typename T>
void checkCpyBW(T* first_ptr, T* second_ptr,
                long bytes, int iterations,
                cudaMemcpyKind kind,
                const char msg[])
{
    chTimerTimestamp start, stop;
    chTimerGetTime(&start);
    for (int j = 0; j < iterations; ++j) {
        cudaMemcpy(first_ptr, second_ptr, bytes, kind);
    }
    cudaDeviceSynchronize();
    chTimerGetTime(&stop);
    double bw = chTimerBandwidth(&start, &stop,
                            bytes*iterations / (double)(1L<<30));
    printf("%s device : %.2f GB/s \n", msg, bw);
}

template <typename T>
void cpyThereAndBack(T* d_mem, T* h_mem, long bytes, int iterations)
{
    for (int j = 0; j < bytes; ++j)
        h_mem[j] = j; // Wrap around :)
    cudaMemset(d_mem, 0, bytes);
    //////// TO DEVICE ///////////////
    checkCpyBW(d_mem, h_mem, bytes, iterations,
               cudaMemcpyHostToDevice, "To");
    //////// FROM DEVICE ////////////
    checkCpyBW(h_mem, d_mem, bytes, iterations,
               cudaMemcpyDeviceToHost, "From");
}

void exercise03()
{
    const int cIterations = 10;
    const long kB = 1024;

    // Jump by powers of 4 from 1 kB up to 1 GB - 10 steps
    for (long i = 1; i <= 1L<<20; i <<= 2) {

        long numberOfBytes = i * kB * sizeof(char);

        printf("Measuring cudaMemcpy for pageable memory %ld KB\n",
                                        numberOfBytes/kB);

        //////// pageable /////////
        char *h_mem, *d_mem;
        cudaMalloc(&d_mem, numberOfBytes);
        h_mem = (char *) malloc(numberOfBytes);

        cpyThereAndBack(d_mem, h_mem, numberOfBytes, cIterations);

        free(h_mem);
        h_mem = NULL;

        ///////// pinned /////////
        cudaMallocHost(&h_mem, numberOfBytes);
        printf("Measuring cudaMemcpy for pinned memory %ld KB\n",
                                            numberOfBytes/kB);

        cpyThereAndBack(d_mem, h_mem, numberOfBytes, cIterations);

        cudaFree(d_mem);
        cudaFreeHost(h_mem);
        d_mem = h_mem = NULL;
    }

}

int main( int argc, char *argv[] )
{

    exercise01();
    exercise02();
    exercise03();

    return 0;
}


\end{lstlisting}

\end{document}
