/*
 *
 * nullKernelAsync.cu
 *
 * Microbenchmark for throughput of asynchronous kernel launch.
 *
 * Build with: nvcc -I ../chLib <options> nullKernelAsync.cu
 * Requires: No minimum SM requirement.
 *
 * Copyright (c) 2011-2012, Archaea Software, LLC.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "chTimer.h"

// how many clocks the kernel took
__device__ clock_t d_Cycles;
// device variable that prevents the kernel from being optimized out
__device__ int d_i;


// Do-nothing kernel
__global__ void NullKernel() { }

// Busy wait kernel that increments a global counter
__global__ void BusyKernel(int iter)
{
    clock_t start_time = clock();

    // Wait for a certain number of iterations
    int i = 0;
    while (i < iter) {
        d_i = i++;
    }

    // Write the number of clock cycles to a device variable
    if (threadIdx.x == 0){
        d_Cycles = clock() - start_time;
    }

}

enum sync { synchronous, asynchronous};
enum vary { thread, block};

double measure(const int n, sync s, vary v, const int cIterations)
{

    dim3 threads(1, 1, 1);
    dim3 blocks(1, 1, 1);
    // messages for the printf
    const char *buff = (s == synchronous ? "synchronous" : "asynchronous");
    const char *buff2 = (v == thread ? "threads" : "blocks");

    // Vary the thing that is supposed to be varied
    if (v == thread){
        threads.x = n;
    } else {
        blocks.x = n;
    }

    printf("Measuring %s launch time for %u %s... ", buff, n, buff2);
    fflush(stdout);

    // Start the clock
    chTimerTimestamp start, stop;
    chTimerGetTime(&start);

    for (int i = 0; i < cIterations; ++i){
        // Invoke kernel
        NullKernel<<<blocks, threads>>>();
        if (s == synchronous){
            // synchronize after each kernel
            cudaDeviceSynchronize();
        }
    }
    if (s == asynchronous){
        // synchronize after all kernels
        cudaDeviceSynchronize();
    }

    // Stop the clock
    chTimerGetTime(&stop);
    double microseconds = 1e6 * chTimerElapsedTime(&start, &stop);
    double usPerLaunch = microseconds / cIterations;

    printf("%.2f us \n", usPerLaunch);

    return usPerLaunch;
}

void exercise01()
{
    const int cIterations = 500000;
    // Special case for 1 thread
    measure(1, synchronous, thread, cIterations);
    measure(1, synchronous, block, cIterations);
    measure(1, asynchronous, thread, cIterations);
    measure(1, asynchronous, block, cIterations);

    for (int i = 32; i <= 1024; i += 32 ){
        measure(i, synchronous, thread, cIterations);
        measure(i, synchronous, block, cIterations);
        measure(i, asynchronous, thread, cIterations);
        measure(i, asynchronous, block, cIterations);
    }
}

double launchBusyKernel(clock_t clocks, const int cIterations,
                        bool verbose=false)
{
    chTimerTimestamp start, stop;
    chTimerGetTime(&start);

    // Run the BusyKernel many times
    for (int i = 0; i < cIterations; ++i){
        BusyKernel<<<1, 1>>>(clocks);
    }
    cudaDeviceSynchronize();
    chTimerGetTime(&stop);

    double microseconds = 1e6 * chTimerElapsedTime(&start, &stop);
    double usPerLaunch = microseconds / cIterations;

    clock_t h_Cycles = 0;
    cudaMemcpyFromSymbol(&h_Cycles, d_Cycles, sizeof(clock_t),
                         0, cudaMemcpyDeviceToHost);

    if (verbose) {
        printf("%.2f us / %ld clocks \n", usPerLaunch, h_Cycles);
    }
    return usPerLaunch;
}

void exercise02()
{
    // Warmup
    BusyKernel<<<1, 1>>>(500);
    cudaDeviceSynchronize();

    const int cIterations = 500000;

    // Reference check for 0 iterations in kernel.
    printf("Reference check\n");
    printf("Running BusyKernel with  0 iterations... ");
    double reference_runtime = launchBusyKernel(0, cIterations, true);
    double runtime = 0.0;
    int iterations = 0;
    printf("\n");

    while (runtime < 2*reference_runtime){
        iterations += 4;
        printf("Running BusyKernel with %2d iterations... ", iterations);
        runtime = launchBusyKernel(iterations, cIterations, true);
    }
    printf("Kernel startup time has doubled.\n");
    printf("Done!\n");
}

template <typename T>
void checkCpyBW(T* first_ptr, T* second_ptr,
                long bytes, int iterations,
                cudaMemcpyKind kind,
                const char msg[])
{
    chTimerTimestamp start, stop;
    chTimerGetTime(&start);
    for (int j = 0; j < iterations; ++j) {
        cudaMemcpy(first_ptr, second_ptr, bytes, kind);
    }
    cudaDeviceSynchronize();
    chTimerGetTime(&stop);
    double bw = chTimerBandwidth(&start, &stop,
                            bytes*iterations / (double)(1L<<30));
    printf("%s device : %.2f GB/s \n", msg, bw);
}

template <typename T>
void cpyThereAndBack(T* d_mem, T* h_mem, long bytes, int iterations)
{
    for (int j = 0; j < bytes; ++j)
        h_mem[j] = j; // Wrap around :)
    cudaMemset(d_mem, 0, bytes);
    //////// TO DEVICE ///////////////
    checkCpyBW(d_mem, h_mem, bytes, iterations,
               cudaMemcpyHostToDevice, "To");
    //////// FROM DEVICE ////////////
    checkCpyBW(h_mem, d_mem, bytes, iterations,
               cudaMemcpyDeviceToHost, "From");
}

void exercise03()
{
    const int cIterations = 10;
    const long kB = 1024;

    // Jump by powers of 4 from 1 kB up to 1 GB - 10 steps
    for (long i = 1; i <= 1L<<20; i <<= 2) {

        long numberOfBytes = i * kB * sizeof(char);

        printf("Measuring cudaMemcpy for pageable memory %ld KB\n",
                                        numberOfBytes/kB);

        //////// pageable /////////
        char *h_mem, *d_mem;
        cudaMalloc(&d_mem, numberOfBytes);
        h_mem = (char *) malloc(numberOfBytes);

        cpyThereAndBack(d_mem, h_mem, numberOfBytes, cIterations);

        free(h_mem);
        h_mem = NULL;

        ///////// pinned /////////
        cudaMallocHost(&h_mem, numberOfBytes);
        printf("Measuring cudaMemcpy for pinned memory %ld KB\n",
                                            numberOfBytes/kB);

        cpyThereAndBack(d_mem, h_mem, numberOfBytes, cIterations);

        cudaFree(d_mem);
        cudaFreeHost(h_mem);
        d_mem = h_mem = NULL;
    }

}

int main( int argc, char *argv[] )
{

//    exercise01();
//    exercise02();
    exercise03();

    return 0;
}
