/*
 *
 * nullKernelAsync.cu
 *
 * Microbenchmark for throughput of asynchronous kernel launch.
 *
 * Build with: nvcc -I ../chLib <options> nullKernelAsync.cu
 * Requires: No minimum SM requirement.
 *
 * Copyright (c) 2011-2012, Archaea Software, LLC.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions 
 * are met: 
 *
 * 1. Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in 
 *    the documentation and/or other materials provided with the 
 *    distribution. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>

#include "chTimer.h"
#include <time.h>

__device__ long d_numberOfCycles, d_busyWait, d_i;

__global__ void NullKernel()
{
}

__global__ void BusyWaitKernel()
{
	clock_t start_time = clock();

	long i = 0;

	while (i < d_busyWait)
	{
		d_i=i++;
		//printf("%u\n",i);
	}

	clock_t stop_time = clock();
	
	if (threadIdx.x == 0)
	{
		//printf("in kernel with threadID: %u ... busyWait is: %u \n", threadIdx.x, d_busyWait);
		d_numberOfCycles = (long)(stop_time - start_time);;
			//end.tv_nsec - begin.tv_nsec;
	}
	

}

void exercise01()
{
	const int cIterations = 100000;

	//measure both kernels varying number of threads per block-------------------------------------------------------------------------------------------------------------
	for (int j = 1; j <= 1024; j++)
	{

		if (j == 1 || j == 102 || j == 204 || j == 306 || j == 408 || j == 510 || j == 612 || j == 714 || j == 816 || j == 918 || j == 1024)
		{

			printf("Measuring asynchronous launch time for %u threads... ", j); fflush(stdout);

			chTimerTimestamp start, stop;

			chTimerGetTime(&start);
			for (int i = 0; i < cIterations; i++)
			{
				NullKernel << <1, j >> >();
			}
			cudaThreadSynchronize();
			chTimerGetTime(&stop);


			double microseconds = 1e6*chTimerElapsedTime(&start, &stop);
			double usPerLaunch = microseconds / (float)cIterations;

			printf("%.2f us\n", usPerLaunch);



			printf("Measuring synchronous launch time for %u threads... ", j); fflush(stdout);

			chTimerGetTime(&start);
			for (int i = 0; i < cIterations; i++)
			{
				NullKernel << <1, j >> >();
				cudaDeviceSynchronize();
			}
			cudaThreadSynchronize();
			chTimerGetTime(&stop);


			microseconds = 1e6*chTimerElapsedTime(&start, &stop);
			usPerLaunch = microseconds / (float)cIterations;

			printf("%.2f us\n", usPerLaunch);

		}
	}

	//measure both kernels varying number of blocks... only difference to the one above
	for (int j = 1; j <= 1024; j++)
	{

		if (j == 1 || j == 102 || j == 204 || j == 306 || j == 408 || j == 510 || j == 612 || j == 714 || j == 816 || j == 918 || j == 1024)
		{

			printf("Measuring asynchronous launch time for %u blocks... ", j); fflush(stdout);

			chTimerTimestamp start, stop;

			chTimerGetTime(&start);
			for (int i = 0; i < cIterations; i++)
			{
				NullKernel << <j, 1 >> >();
			}
			cudaThreadSynchronize();
			chTimerGetTime(&stop);


			double microseconds = 1e6*chTimerElapsedTime(&start, &stop);
			double usPerLaunch = microseconds / (float)cIterations;

			printf("%.2f us\n", usPerLaunch);



			printf("Measuring synchronous launch time for %u blocks... ", j); fflush(stdout);

			chTimerGetTime(&start);
			for (int i = 0; i < cIterations; i++)
			{
				NullKernel << <j, 1 >> >();
				cudaDeviceSynchronize();
			}
			cudaThreadSynchronize();
			chTimerGetTime(&stop);


			microseconds = 1e6*chTimerElapsedTime(&start, &stop);
			usPerLaunch = microseconds / (float)cIterations;

			printf("%.2f us\n", usPerLaunch);

		}
	}

}

void exercise02()
{
	//measure break even clock cycle number--------------------------------------------------------------------------------------------------- 

	const int cIterations = 1000000;
	bool breakEven = true;

	while (breakEven)
	{
		//measure reference time for asynchronous kernel launch
		printf("Measuring asynchronous launch time for reference with busyWait = 0... "); fflush(stdout);

		//set number of iterations to do in the kernel
		long h_busyWait = 0;

		cudaMemcpyToSymbol(d_busyWait, &h_busyWait, sizeof(long), 0, cudaMemcpyHostToDevice);

		chTimerTimestamp start, stop;

		chTimerGetTime(&start);
		for (int i = 0; i < cIterations; i++)
		{
			BusyWaitKernel << <1, 1 >> >();
		}
		cudaThreadSynchronize();
		chTimerGetTime(&stop);

		long h_numberOfCycles = 0;
		cudaMemcpyFromSymbol(&h_numberOfCycles, d_numberOfCycles, sizeof(long), 0, cudaMemcpyDeviceToHost);

		double microseconds = 1e6*chTimerElapsedTime(&start, &stop);
		double RefusPerLaunch = microseconds / (float)cIterations;

		printf("%.2f us\n", RefusPerLaunch);
		printf("Number of clock cycles %u \n", h_numberOfCycles);

		//set number of iterations to do in the kernel
		h_busyWait = 80;

		cudaMemcpyToSymbol(d_busyWait, &h_busyWait, sizeof(long), 0, cudaMemcpyHostToDevice);

		printf("Measuring asynchronous launch time for %u busy-iterations... ", h_busyWait); fflush(stdout);


		chTimerGetTime(&start);
		for (int i = 0; i < cIterations; i++)
		{
			BusyWaitKernel << <1, 1 >> >();
		}
		cudaThreadSynchronize();
		chTimerGetTime(&stop);


		//cudaMemcpyToSymbol(&h_numberOfCycles, d_numberOfCycles, sizeof(long), 0, cudaMemcpyDeviceToHost);
		//cudaMemcpy(&h_numberOfCycles, &d_numberOfCycles, sizeof(long), cudaMemcpyDeviceToHost);

		cudaMemcpyFromSymbol(&h_numberOfCycles, d_numberOfCycles, sizeof(long), 0, cudaMemcpyDeviceToHost);

		microseconds = 1e6*chTimerElapsedTime(&start, &stop);
		double usPerLaunch = microseconds / (float)cIterations;

		printf("%.2f us\n", usPerLaunch);
		printf("Number of clock cycles %u \n", h_numberOfCycles);

		breakEven = false;

	}

}

void exercise03()
{
	//compare data transfer methods ----------------------------------------------------------------------------------------------------------------
	const int cIterations = 1000;

	int numberOfkB = 1;

	for (int j = 0; j < 10; j++)
	{

		int numberOfBytes = numberOfkB * 1024;

		int numberOfFloats = numberOfBytes / sizeof(float);


		printf("Measuring cudaMemcpy on pageable memory for %u kB, transferring %u floats... \n", numberOfkB, numberOfFloats); fflush(stdout);

		float *hmem, *dmem;

		cudaMalloc(&dmem, numberOfFloats*sizeof (float)); // Allocate GPU memory
		hmem = (float*)malloc(numberOfFloats*sizeof (float)); // Allocate CPU memory

		for (int i = 0; i < numberOfFloats; i++)
			hmem[i] = (float)i;

		chTimerTimestamp start, stop;

		chTimerGetTime(&start);


		for (int i = 0; i < cIterations; i++)
		{
			cudaMemcpy(dmem, hmem, numberOfFloats*sizeof (float), cudaMemcpyHostToDevice);
		}

		cudaDeviceSynchronize();
		chTimerGetTime(&stop);


		double microseconds = 1e6*chTimerElapsedTime(&start, &stop);
		double usPerCopy = microseconds / (float)cIterations;

		printf("To device: %.2f us\n", usPerCopy);
		printf("To device %.2f GB/s \n", chTimerBandwidth(&start, &stop,
			numberOfBytes*cIterations / (double)(1L << 30)));



		chTimerGetTime(&start);

		for (int i = 0; i < cIterations; i++)
		{
			cudaMemcpy(hmem, dmem, numberOfFloats*sizeof (float), cudaMemcpyDeviceToHost);
		}

		cudaDeviceSynchronize();
		chTimerGetTime(&stop);


		microseconds = 1e6*chTimerElapsedTime(&start, &stop);
		usPerCopy = microseconds / (float)cIterations;

		//printf("%.2f float 1 \n", hmem[1]);
		printf("From device: %.2f us\n", usPerCopy);
		printf("From device %.2f GB/s \n", chTimerBandwidth(&start, &stop,
			numberOfBytes*cIterations / (double)(1L << 30)));

		free(hmem);
		hmem = NULL;


		//measure on pinned memory---------------------------------------------------------------------------------------------------------------------------------------------------


		printf("Measuring cudaMemcpy on pinned memory for %u kB, transferring %u floats... \n", numberOfkB, numberOfFloats); fflush(stdout);

		//float *hmem, *dmem;

		cudaMallocHost(&hmem, numberOfFloats*sizeof (float)); // Allocate CPU memory

		//cudaMalloc(&dmem, numberOfFloats*sizeof (float)); // Allocate GPU memory

		for (int i = 0; i < numberOfFloats; i++)
			hmem[i] = (float)i;


		chTimerGetTime(&start);


		for (int i = 0; i < cIterations; i++)
		{
			cudaMemcpy(dmem, hmem, numberOfFloats*sizeof (float), cudaMemcpyHostToDevice);
		}

		cudaDeviceSynchronize();
		chTimerGetTime(&stop);


		microseconds = 1e6*chTimerElapsedTime(&start, &stop);
		usPerCopy = microseconds / (float)cIterations;

		printf("To device: %.2f us\n", usPerCopy);
		printf("To device %.2f GB/s \n", chTimerBandwidth(&start, &stop,
			numberOfBytes*cIterations / (double)(1L << 30)));



		chTimerGetTime(&start);

		for (int i = 0; i < cIterations; i++)
		{
			cudaMemcpy(hmem, dmem, numberOfFloats*sizeof (float), cudaMemcpyDeviceToHost);
		}

		cudaDeviceSynchronize();
		chTimerGetTime(&stop);


		microseconds = 1e6*chTimerElapsedTime(&start, &stop);
		usPerCopy = microseconds / (float)cIterations;

		//printf("%.2f float 1 \n", hmem[1]);
		printf("From device: %.2f us\n", usPerCopy);
		printf("From device %.2f GB/s \n", chTimerBandwidth(&start, &stop,
			numberOfBytes*cIterations / (double)(1L << 30)));

		cudaFree(dmem);
		cudaFreeHost(hmem);
		dmem = hmem = NULL;

		numberOfkB = numberOfkB + 1048576 / 10;
	}

}

int main(int argc, char *argv[])
{

	//measure both kernels varying number of threads per block-------------------------------------------------------------------------------------------------------------
	exercise01();

	//measure break even clock cycle number--------------------------------------------------------------------------------------------------- 
	//exercise02();
	

    //compare data transfer methods ----------------------------------------------------------------------------------------------------------------
	//exercise03();

    return 0;
}
