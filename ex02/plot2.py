#!/usr/bin/env python
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker

import process2

name = "bw_timings.json"
df = pd.read_json(name)

df.sort_index(inplace=True)

print df

fig = plt.figure(figsize=(6,3.5))
ax1 = plt.subplot2grid((1,3), (0, 0), colspan=2)
df.plot(ax=ax1, title="Data transfer bandwidth", marker=".")
plt.ylabel(r"Transfer bandwidth $\frac{\mathrm{GB}}{\mathrm{s}}$")
plt.xlabel(r"Size $\mathrm{kB}$")
plt.xlim((1, 2**20))
plt.xscale("log",basex=2)
ax1.xaxis.set_ticks(np.logspace(base=2,start=0, stop=20, num=6))
ax1.xaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter())
plt.legend(fontsize="small",
           bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.0)
plt.tight_layout()
plt.savefig("data_transfer.pdf")
