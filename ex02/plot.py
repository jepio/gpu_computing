#!/usr/bin/env python
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import process
import sys

name = sys.argv[1] if len(sys.argv) == 2 else "startup_timings.out"
outname = "startup_timings.json"
process.process_file(name, outname)
df = pd.read_json(outname)
df.sort_index(inplace=True)

print df

fig = plt.figure(figsize=(6,3.5))
ax1 = plt.subplot2grid((1,3), (0, 0), colspan=2)
df.plot(ax=ax1, title="Kernel Startup Time", marker=".")
plt.xlabel(r"Number")
plt.ylabel(r"Time ($\mu s$)")
plt.xlim((0, 1024))
ax1.xaxis.set_ticks(np.linspace(0,1024,9, endpoint=True))
#ax1.set_xscale("log", basex=2)
plt.legend(fontsize="small",
           bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.0)
plt.tight_layout()
plt.savefig("kernel_startup.pdf")
