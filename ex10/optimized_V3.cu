/*************************************************************************************************
*
*        Computer Engineering Group, Heidelberg University - GPU Computing Exercise 09
*
*                           Group : Bernd Epding, Jeremi Piotrowski, Ludwig Richter
*
*                            File : main.cu
*
*                         Purpose : Stencil Code
*
*************************************************************************************************/

#include <cuda_profiler_api.h> 

#include <cmath>
#include <ctime>
#include <iostream>
#include <cstdlib>
#include "../inc/chCommandLine.h"
#include "../inc/chTimer.hpp"
#include <cstdio>
#include <iomanip>

const static int DEFAULT_NUM_ELEMENTS = 1024;
const static int DEFAULT_NUM_ITERATIONS = 5;
const static int DEFAULT_BLOCK_DIM = 128;

//
// Structures
struct StencilArray_t {
	float* array;
	int    size; // size == width == height
};

//define texture reference
texture<float, 1, cudaReadModeElementType> tex;

//
// Function Prototypes
//
void printHelp(char *);
void printArray(const StencilArray_t&);

bool ResultsEqual(float gpuRes, float cpuRes)
{
	if (fabs((gpuRes - cpuRes) / (gpuRes + cpuRes)) > 1E-04) {
		return false;
	}

	return true;
}

//
// Naive Stencil Code Kernel for the speed calculation
//
__global__ void
simpleStencil_Kernel(const StencilArray_t d_array, StencilArray_t d_array_temp)
{
	const int overall_id = threadIdx.x + blockDim.x * blockIdx.x;
	if (overall_id < d_array.size * d_array.size)
	{
		d_array_temp.array[overall_id] = d_array.array[overall_id];
		if (overall_id >= d_array.size && 						// exclude first line
			overall_id < d_array.size * (d_array.size - 1) && 	// exclude last line
			overall_id % d_array.size != 0 && 					// exclude first column
			overall_id % d_array.size != d_array.size - 1)		// exclude last column
		{
			d_array_temp.array[overall_id] += 0.24f * (
				-4.0f * d_array.array[overall_id] + 	    // self
				d_array.array[overall_id - d_array.size] +  // top
				d_array.array[overall_id - 1] + 		 	// left
				d_array.array[overall_id + 1] +			 	// right
				d_array.array[overall_id + d_array.size]);  // bottom
		}
	}
}

// [NOT WORKING!]
//
// Optimized Stencil Code Kernel for the speed calculation:
//   - Using Shared Memory by blocking
//
__global__ void
optStencil_kernel(const StencilArray_t d_array, StencilArray_t d_array_temp)
{
	extern __shared__ float shared[];

	const int i = threadIdx.x + blockDim.x * blockIdx.x;
	const int j = threadIdx.y + blockDim.x * blockIdx.y;
	const int overall_id = i + j * d_array.size;
	const int local_id = threadIdx.x + threadIdx.y * blockDim.x;

	if (overall_id < d_array.size * d_array.size)
	{
		// Load block into shared memory
		shared[local_id] = d_array.array[overall_id];
		__syncthreads();

		// Check if the current point is not at the border of the array
		float val = 0.0f;
		if (j > 0 && j < d_array.size - 1 && i > 0 && i < d_array.size - 1)
		{

			// Get the neighbouring points; either from shared memory or global memory
			const float n = (threadIdx.y > 0) ? shared[local_id - blockDim.x] : d_array.array[overall_id - d_array.size];
			const float w = (threadIdx.x > 0) ? shared[local_id - 1] : d_array.array[overall_id - 1];
			const float s = (threadIdx.y < blockDim.x - 1) ? shared[local_id + blockDim.x] : d_array.array[overall_id + d_array.size];
			const float e = (threadIdx.x < blockDim.x - 1) ? shared[local_id + 1] : d_array.array[overall_id + 1];
			val = 0.24f * (-4.0f * shared[local_id] + n + w + s + e);

		}
		// Update the current element*/
		d_array_temp.array[overall_id] = shared[local_id] + val;
	}
}


__global__ void
optStencil_kernel3(StencilArray_t d_array, StencilArray_t d_array_temp)
{
	extern __shared__ float shared[];

	const int tId = threadIdx.x;
	const int bIdx = blockIdx.x; const int bIdy = blockIdx.y; const int bDim = blockDim.x;

	const int overall_id = (bIdy + 1) * d_array.size + 1 + bIdx * bDim + tId;						//corresponding to place in d_array (border excluded)
	const int overall_block_start = (bIdy + 1) * d_array.size + 1 + bIdx * bDim;
	
	//init sharedMem ... don�t worry about halo
	/*
	for (int i = tId; i < (bDim + 2) * 3; i += bDim)
	{
		shared[i] = 0.0f;
	}
	__syncthreads();
	*/

	//load starting halo

#pragma unroll 6
	for (int j = 0; j < 3; j++) // 3rows
	{
#pragma unroll 6
		for (int i = tId; i < bDim + 2; i += bDim) //thread 0,1 load two elements this is not good
		{
			if ((overall_block_start + (-1 + j)*(d_array.size) - 1 + i) % d_array.size < d_array.size) //guard right side
			{
				shared[j*(bDim + 2) + i] = d_array.array[overall_block_start + (-1 + j)*(d_array.size) - 1 + i];
			}
		}
	}

	__syncthreads();

	//compute stencil on sharedMem

	if (bIdx * bDim + tId < d_array.size - 2)
	{
		float val = 0.0f;

		int id_in_shared = tId + bDim + 3;

		// Get the neighbouring points; either from shared memory or global memory
		const float n = shared[id_in_shared - (bDim + 2)];
		const float w = shared[id_in_shared - 1];
		const float s = shared[id_in_shared + (bDim + 2)];
		const float e = shared[id_in_shared + 1];
		val = 0.24f * (-4.0f * shared[id_in_shared] + n + w + s + e);

		// Update the current element*/
		d_array_temp.array[overall_id] = shared[id_in_shared] + val;
	}

	
	__syncthreads();

	int activeSharedRow = 0;
	for (int k = 1; k < d_array.size - 3; k++) // walk top down through array
	{
		//load next line of data and place it in sharedMem row that is not used anymore 
		for (int i = tId; i < bDim + 2; i += bDim) //thread 0,1 load two elements this is not good but cant help it
		{
			if ((overall_block_start + (k + 1)*(d_array.size) - 1 + i) % d_array.size < d_array.size) //guard right side
			{
				shared[(activeSharedRow%3)*(bDim + 2) + i] = d_array.array[overall_block_start + (k+1)*(d_array.size) - 1 + i];
			}
		}
		__syncthreads();

		activeSharedRow++;

		//stencil on next row

		if (activeSharedRow == 0 && bIdx * bDim + tId < d_array.size - 2)
		{
			float val = 0.0f;

			int id_in_shared = tId + 2*(bDim + 2) +1;

			// Get the neighbouring points; either from shared memory or global memory
			const float n = shared[id_in_shared - (bDim + 2)];
			const float w = shared[id_in_shared - 1];
			const float s = shared[id_in_shared - 2*(bDim + 2)];
			const float e = shared[id_in_shared + 1];
			val = 0.24f * (-4.0f * shared[id_in_shared] + n + w + s + e);

			// Update the current element*/
			d_array_temp.array[overall_id +k*d_array.size] = shared[id_in_shared] + val;
		}

		if (activeSharedRow == 1 && bIdx * bDim + tId < d_array.size - 2)
		{
			float val = 0.0f;

			int id_in_shared = tId + 1;

			// Get the neighbouring points; either from shared memory or global memory
			const float n = shared[id_in_shared + 2*(bDim + 2)];
			const float w = shared[id_in_shared - 1];
			const float s = shared[id_in_shared + (bDim + 2)];
			const float e = shared[id_in_shared + 1];
			val = 0.24f * (-4.0f * shared[id_in_shared] + n + w + s + e);

			// Update the current element*/
			d_array_temp.array[overall_id + k*d_array.size] = shared[id_in_shared] + val;
		}

		if (activeSharedRow == 2 && bIdx * bDim + tId < d_array.size - 2)
		{
			float val = 0.0f;

			int id_in_shared = tId + bDim + 3;

			// Get the neighbouring points; either from shared memory or global memory
			const float n = shared[id_in_shared - (bDim + 2)];
			const float w = shared[id_in_shared - 1];
			const float s = shared[id_in_shared + (bDim + 2)];
			const float e = shared[id_in_shared + 1];
			val = 0.24f * (-4.0f * shared[id_in_shared] + n + w + s + e);

			// Update the current element*/
			d_array_temp.array[overall_id + k*d_array.size] = shared[id_in_shared] + val;
		}

		__syncthreads();
	}
	
}


//using textureMem

__global__ void
optStencil_kernel_tex(StencilArray_t d_array, StencilArray_t d_array_temp)
{
	extern __shared__ float shared[];

	const int tId = threadIdx.x;
	const int bIdx = blockIdx.x; const int bIdy = blockIdx.y; const int bDim = blockDim.x;

	const int overall_id = (bIdy + 1) * d_array.size + 1 + bIdx * bDim + tId;						//corresponding to place in d_array (border excluded)
	const int overall_block_start = (bIdy + 1) * d_array.size + 1 + bIdx * bDim;
	
	//init sharedMem ... don�t worry about halo
	/*
	for (int i = tId; i < (bDim + 2) * 3; i += bDim)
	{
		shared[i] = 0.0f;
	}
	__syncthreads();
	*/

	//load starting halo

#pragma unroll 6
	for (int j = 0; j < 3; j++) // 3rows
	{
#pragma unroll 6
		for (int i = tId; i < bDim + 2; i += bDim) //thread 0,1 load two elements this is not good
		{
			if ((overall_block_start + (-1 + j)*(d_array.size) - 1 + i) % d_array.size < d_array.size) //guard right side
			{
				//shared[j*(bDim + 2) + i] = d_array.array[overall_block_start + (-1 + j)*(d_array.size) - 1 + i];
				shared[j*(bDim + 2) + i] = tex1Dfetch(tex,overall_block_start + (-1 + j)*(d_array.size) - 1 + i);
			}
		}
	}

	__syncthreads();

	//compute stencil on sharedMem

	if (bIdx * bDim + tId < d_array.size - 2)
	{
		float val = 0.0f;

		int id_in_shared = tId + bDim + 3;

		// Get the neighbouring points; either from shared memory or global memory
		const float n = shared[id_in_shared - (bDim + 2)];
		const float w = shared[id_in_shared - 1];
		const float s = shared[id_in_shared + (bDim + 2)];
		const float e = shared[id_in_shared + 1];
		val = 0.24f * (-4.0f * shared[id_in_shared] + n + w + s + e);

		// Update the current element*/
		d_array_temp.array[overall_id] = shared[id_in_shared] + val;
	}

	
	__syncthreads();

	int activeSharedRow = 0;
	for (int k = 1; k < d_array.size - 3; k++) // walk top down through array
	{
		//load next line of data and place it in sharedMem row that is not used anymore 
		for (int i = tId; i < bDim + 2; i += bDim) //thread 0,1 load two elements this is not good but cant help it
		{
			if ((overall_block_start + (k + 1)*(d_array.size) - 1 + i) % d_array.size < d_array.size) //guard right side
			{
				shared[(activeSharedRow % 3)*(bDim + 2) + i] = tex1Dfetch(tex, overall_block_start + (k + 1)*(d_array.size) - 1 + i);
			}
		}
		__syncthreads();

		activeSharedRow++;

		//stencil on next row

		if (activeSharedRow == 0 && bIdx * bDim + tId < d_array.size - 2)
		{
			float val = 0.0f;

			int id_in_shared = tId + 2*(bDim + 2) +1;

			// Get the neighbouring points; either from shared memory or global memory
			const float n = shared[id_in_shared - (bDim + 2)];
			const float w = shared[id_in_shared - 1];
			const float s = shared[id_in_shared - 2*(bDim + 2)];
			const float e = shared[id_in_shared + 1];
			val = 0.24f * (-4.0f * shared[id_in_shared] + n + w + s + e);

			// Update the current element*/
			d_array_temp.array[overall_id +k*d_array.size] = shared[id_in_shared] + val;
		}

		if (activeSharedRow == 1 && bIdx * bDim + tId < d_array.size - 2)
		{
			float val = 0.0f;

			int id_in_shared = tId + 1;

			// Get the neighbouring points; either from shared memory or global memory
			const float n = shared[id_in_shared + 2*(bDim + 2)];
			const float w = shared[id_in_shared - 1];
			const float s = shared[id_in_shared + (bDim + 2)];
			const float e = shared[id_in_shared + 1];
			val = 0.24f * (-4.0f * shared[id_in_shared] + n + w + s + e);

			// Update the current element*/
			d_array_temp.array[overall_id + k*d_array.size] = shared[id_in_shared] + val;
		}

		if (activeSharedRow == 2 && bIdx * bDim + tId < d_array.size - 2)
		{
			float val = 0.0f;

			int id_in_shared = tId + bDim + 3;

			// Get the neighbouring points; either from shared memory or global memory
			const float n = shared[id_in_shared - (bDim + 2)];
			const float w = shared[id_in_shared - 1];
			const float s = shared[id_in_shared + (bDim + 2)];
			const float e = shared[id_in_shared + 1];
			val = 0.24f * (-4.0f * shared[id_in_shared] + n + w + s + e);

			// Update the current element*/
			d_array_temp.array[overall_id + k*d_array.size] = shared[id_in_shared] + val;
		}

		__syncthreads();
	}
	
}

//
// Main
//
int
main(int argc, char * argv[])
{
	bool showHelp = chCommandLineGetBool("h", argc, argv);
	if (!showHelp) {
		showHelp = chCommandLineGetBool("help", argc, argv);
	}

	bool printIt = chCommandLineGetBool("print", argc, argv);
	bool optimized = chCommandLineGetBool("opt", argc, argv);
	bool comp = chCommandLineGetBool("comp", argc, argv);
	bool use_tex = chCommandLineGetBool("tex", argc, argv);

	if (showHelp) {
		printHelp(argv[0]);
		exit(0);
	}

	ChTimer memCpyH2DTimer, memCpyD2HTimer;
	ChTimer kernelTimer;

	//
	// Allocate Memory
	//
	int numElements = 0;
	chCommandLineGet<int>(&numElements, "s", argc, argv);
	chCommandLineGet<int>(&numElements, "size", argc, argv);
	numElements = numElements != 0 ? numElements : DEFAULT_NUM_ELEMENTS;



	//
	// Host Memory
	//
	StencilArray_t h_array;
	h_array.size = numElements;
	int arraySize = h_array.size*h_array.size;

	cudaMallocHost(&(h_array.array), static_cast<size_t>
		(arraySize * sizeof(*(h_array.array))));

	// Init Particles
	for (int i = 0; i < arraySize; i++) {
		h_array.array[i] = 0;
	}

	//set heat source at "top row" that is 0 to h_array.size-1
	//heat at (h_array.size/4) to ((h_array.size/4) * 3)-1
	//if  h_array.size not divisible by 4... don't bother should still work
	for (int i = (h_array.size / 4); i <((h_array.size / 4) * 3); i++) {
		h_array.array[i] = 127.0f;
	}

	// Device Memory
	StencilArray_t d_array;
	d_array.size = h_array.size;

	cudaMalloc(&(d_array.array),
		static_cast<size_t>(arraySize * sizeof(*h_array.array)));

	StencilArray_t d_array_temp;
	d_array_temp.size = h_array.size;

	cudaMalloc(&(d_array_temp.array),
		static_cast<size_t>(arraySize * sizeof(*h_array.array)));

	if (h_array.array == NULL || d_array.array == NULL || d_array_temp.array == NULL) {
		std::cout << "\033[31m***" << std::endl
			<< "*** Error - Memory allocation failed" << std::endl
			<< "***\033[0m" << std::endl;
		exit(-1);
	}

	//
	// Copy Data to the Device
	//

	memCpyH2DTimer.start();

	cudaMemcpy(d_array.array, h_array.array,
		static_cast<size_t>(arraySize * sizeof(*d_array.array)),
		cudaMemcpyHostToDevice);

	cudaMemcpy(d_array_temp.array, h_array.array,
		static_cast<size_t>(arraySize * sizeof(*d_array.array)),
		cudaMemcpyHostToDevice);

	memCpyH2DTimer.stop();

	//if wanted bind deviceMem to texture
	if (use_tex)
	{
		cudaBindTexture(NULL,tex, d_array.array, arraySize * sizeof(*d_array.array));
	}

	//
	// Get Kernel Launch Parameters
	//
	int blockSize = 0, gridSize = 0, numIterations = 0;

	// Number of Iterations 
	chCommandLineGet<int>(&numIterations, "i", argc, argv);
	chCommandLineGet<int>(&numIterations, "num-iterations", argc, argv);
	numIterations = numIterations != 0 ? numIterations : DEFAULT_NUM_ITERATIONS;

	// Block Dimension / Threads per Block
	chCommandLineGet<int>(&blockSize, "t", argc, argv);
	chCommandLineGet<int>(&blockSize, "threads-per-block", argc, argv);
	blockSize = blockSize != 0 ? blockSize : DEFAULT_BLOCK_DIM;

	if (blockSize > 1024) {
		std::cout << "\033[31m***" << std::endl
			<< "*** Error - The number of threads per block is too big!" << std::endl
			<< "***\033[0m" << std::endl;
		exit(-1);
	}

	/*
	if (optimized) {
	// Check if blockSize is a quadratic number
	if ((int)std::sqrt(blockSize) * (int)std::sqrt(blockSize) != blockSize) {
	std::cout << "\033[31m***" << std::endl
	<< "*** Error - The number of threads per block must be a quadratic number!" << std::endl
	<< "***\033[0m" << std::endl;
	exit(-1);
	}

	// Reassign blockSize to its square root, since we'll create two-dimensional blocks of threads
	blockSize = (int)std::sqrt(blockSize);
	}*/

	// Bound blockSize to numElements
	blockSize = (blockSize > h_array.size) ? h_array.size : blockSize;

	dim3 grid_dim;
	dim3 block_dim;
	if (!optimized) {
		block_dim = dim3(blockSize);
		gridSize = ceil(static_cast<float>(arraySize) / static_cast<float>(blockSize));
		grid_dim = dim3(gridSize);
	}
	else {
		block_dim = dim3(blockSize);
		gridSize = ceil(static_cast<float>(h_array.size) / static_cast<float>(blockSize)); // this needs dealing with overlap on right side of array...
		//grid_dim = dim3(gridSize,h_array.size -2);
		grid_dim = dim3(gridSize);
	}

	std::cout << "Grid-Size: x: " << grid_dim.x << "y: " << grid_dim.y << std::endl;
	std::cout << "Block-Size: " << blockSize << std::endl;

	kernelTimer.start();

	if (h_array.size <= 12) // only show small arrays
	{
		for (int j = 0; j < h_array.size; j++)
		{
			for (int i = j * h_array.size; i < j * h_array.size + h_array.size; i++) {
				std::cout << h_array.array[i] << " ";
			}
			std::cout << std::endl;
		}
	}

	for (int i = 0; i < numIterations; ++i) 
	{
		if (!optimized) {
			simpleStencil_Kernel <<<grid_dim, block_dim >>>(d_array, d_array_temp);
			// Swap device memory pointers
			float* tmp = d_array.array;
			d_array.array = d_array_temp.array;
			d_array_temp.array = tmp;
		}
		else if (optimized && use_tex)
		{
			//std::cout << "Using textureMem" << std::endl;
			optStencil_kernel3 <<<grid_dim, block_dim, (3 * (block_dim.x + 2)) * sizeof(float) >>>(d_array, d_array_temp);

			cudaUnbindTexture(tex);
			float* tmp = d_array.array;
			d_array.array = d_array_temp.array;
			d_array_temp.array = tmp;
			cudaBindTexture(NULL, tex, d_array.array, arraySize * sizeof(*d_array.array));
		}
		else {
			//optStencil_kernel<<<grid_dim, block_dim, block_dim.x * block_dim.y * sizeof(float)>>>(d_array, d_array_temp);
			//optStencil_kernel2 <<<grid_dim, block_dim, (((block_dim.x + 4) * 5) + (3 * (block_dim.x + 2))) * sizeof(float) >>>(d_array, d_array_temp);
			optStencil_kernel3 <<<grid_dim, block_dim, (3 * (block_dim.x + 2)) * sizeof(float) >>>(d_array, d_array_temp);
			//			i+=1;
			// Swap device memory pointers
			float* tmp = d_array.array;
			d_array.array = d_array_temp.array;
			d_array_temp.array = tmp;
		}
	}

	// Synchronize
	cudaDeviceSynchronize();

	// Check for Errors
	cudaError_t cudaError = cudaGetLastError();
	if (cudaError != cudaSuccess) {
		std::cout << "\033[31m***" << std::endl
			<< "***ERROR*** " << cudaError << " - " << cudaGetErrorString(cudaError)
			<< std::endl
			<< "***\033[0m" << std::endl;
		exit(-1);
	}

	kernelTimer.stop();

	// Compare the results of the unoptimized and optimized stencil kernels for debugging
	if (comp && optimized) {
		block_dim = dim3(blockSize);
		gridSize = ceil(static_cast<float>(arraySize) / static_cast<float>(blockSize));
		dim3 grid_dim = dim3(gridSize);
		StencilArray_t h_array_opt;
		h_array_opt.size = numElements;
		cudaMallocHost(&(h_array_opt.array), static_cast<size_t>
			(arraySize * sizeof(*(h_array_opt.array))));
		cudaMemcpy(h_array_opt.array, d_array.array,
			static_cast<size_t>(arraySize * sizeof(*(h_array_opt.array))),
			cudaMemcpyDeviceToHost);
		cudaMemcpy(d_array.array, h_array.array,
			static_cast<size_t>(arraySize * sizeof(*d_array.array)),
			cudaMemcpyHostToDevice);
		for (int i = 0; i < numIterations; ++i) {
			simpleStencil_Kernel <<<grid_dim, block_dim >>>(d_array, d_array_temp);
			float* tmp = d_array.array;
			d_array.array = d_array_temp.array;
			d_array_temp.array = tmp;
		}
		cudaDeviceSynchronize();
		cudaError_t cudaError = cudaGetLastError();
		if (cudaError != cudaSuccess) {
			std::cout << "\033[31m***" << std::endl
				<< "***ERROR*** " << cudaError << " - " << cudaGetErrorString(cudaError)
				<< std::endl
				<< "***\033[0m" << std::endl;
			exit(-1);
		}
		cudaMemcpy(h_array.array, d_array.array,
			static_cast<size_t>(arraySize * sizeof(*(h_array.array))),
			cudaMemcpyDeviceToHost);
		for (int i = 0; i < arraySize; ++i) {
			if (!ResultsEqual(h_array_opt.array[i], h_array.array[i])) {
				std::cout << "\033[31m***" << std::endl
					<< "***ERROR*** " << "Arrays for unoptimized and optimized kernels are not similar!"
					<< std::endl
					<< "***\033[0m" << std::endl;
				exit(-1);
			}
		}
		std::cout << "Comparison was successful!" << std::endl;
		cudaFreeHost(h_array_opt.array);
	}

	/* 	if (printIt) {
	std::cout << "Array before stencil computations:" << std::endl;
	printArray(h_array);
	} */

	//
	// Copy Back Data
	//
	memCpyD2HTimer.start();

	cudaMemcpy(h_array.array, d_array.array,
		static_cast<size_t>(arraySize * sizeof(*(h_array.array))),
		cudaMemcpyDeviceToHost);

	memCpyD2HTimer.stop();

	if (printIt) {
		std::cout << "Array after stencil computations:" << std::endl;
		printArray(h_array);
	}

	if (h_array.size <= 12) // only show small arrays
	{
		for (int j = 0; j < h_array.size; j++)
		{
			for (int i = j * h_array.size; i < j * h_array.size + h_array.size; i++) {
				std::cout << h_array.array[i] << " ";
			}
			std::cout << std::endl;
		}
	}

	// Free Memory
	cudaFreeHost(h_array.array);
	cudaFree(d_array.array);

	// Print Meassurement Results 
	// N;H2D Time (ms); H2D Bandwidth (GB/s);D2H Time (ms); D2H Bandwidth (GB/s); Kernel Time (ms)
	std::cout << numElements << ";" << 1e3 * memCpyH2DTimer.getTime() << ";"
		<< 1e-9 * memCpyH2DTimer.getBandwidth(numElements * sizeof(*h_array.array)) << ";"
		<< 1e-9 * memCpyD2HTimer.getBandwidth(numElements * sizeof(*h_array.array)) << ";"
		<< 1e3 * kernelTimer.getTime() << std::endl;

	cudaProfilerStop();
	return 0;
}

void
printHelp(char * argv)
{
	std::cout << "Help:" << std::endl
		<< "  Usage: " << std::endl
		<< "  " << argv << " [-s <num-elements>] [-i <num-iterations] [-t <threads_per_block>] [-print]"
		<< std::endl
		<< "" << std::endl
		<< "  -i <num-iterations>|--num-iterations <num-iterations>"
		<< std::endl
		<< "    The number of iterations the stencil kernel should be applied." << std::endl
		<< "" << std::endl
		<< "  -s <width-and-height>|--size <width-and-height>" << std::endl
		<< "    THe width and the height of the array - should be divisible by 4... no must I guess" << std::endl
		<< "" << std::endl
		<< "  -t <threads_per_block>|--threads-per-block <threads_per_block>"
		<< std::endl
		<< "    The number of threads per block" << std::endl
		<< "" << std::endl
		<< "  -opt "
		<< std::endl
		<< "    Use optimized kernel." << std::endl
		<< "" << std::endl
		<< "  -print "
		<< std::endl
		<< "    Print the array before and after the stencil-operations." << std::endl
		<< "" << std::endl
		<< "  -comp "
		<< std::endl
		<< "    Compare the results of the unoptimized and optimized stencil kernels. Has only effect if -opt is used." << std::endl
		<< "" << std::endl;
}

void
printArray(const StencilArray_t& array)
{
	for (int j = 0; j < array.size; j++)
	{
		for (int i = j * array.size; i < (j + 1) * array.size; ++i) {
			std::cout << std::setprecision(3) << std::setw(8) << array.array[i] << " ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}
