import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

plt.rc("text", usetex=True)
plt.rc("font", family="sans-serif")
plt.rc("font", size=12, serif="Helvetica")
plt.rc("legend", fontsize=11)

def linear(x, a, b):
    return a*x + b

def parabolic(x, a, b, c):
    return a*(x - b) ** 2 + c

x, y = np.genfromtxt("a1.col", unpack=True)
x2, y2 = np.genfromtxt("a2.col", unpack=True)

x /= 1000
x2 /= 1000

y /= 1000
y2 /= 1000

scale = 1.3
plt.figure(figsize=(5/scale, 4/scale))
plt.plot(x, y, "-", label="Unoptimized")
plt.plot(x2, y2, "-", label="Optimized")
plt.legend(loc=0, frameon=False)
plt.xlabel("Number of elements / 1000")
plt.ylabel("Time / s")
plt.title("Comparison GPU")

plt.tight_layout()
plt.savefig("a2.eps")

plt.figure(figsize=(5/scale, 4/scale))
plt.plot(x, y2 / y, ".-", label="Unoptimized")
plt.xlabel("Number of elements / 1000")
plt.ylabel("Ratio")
plt.ylim(0, 1)
plt.title("Execution time ratio")
plt.tight_layout()
plt.savefig("a2-2.eps")


