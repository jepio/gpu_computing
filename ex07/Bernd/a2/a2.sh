#!/usr/bin/env bash
#SBATCH --gres=gpu
#SBATCH -o a2.txt

thread=96
bodies=$(seq 4 1 18)

for b in $bodies
do
    b=$((1<<b))
    ../a2_packed_sharedMem_noConstMem -p -s $b -t $thread
done
