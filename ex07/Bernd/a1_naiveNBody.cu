/******************************************************************************
 *
 *           XXXII Heidelberg Physics Graduate Days - GPU Computing
 *
 *                 Gruppe : TODO
 *
 *                   File : main.cu
 *
 *                Purpose : n-Body Computation
 *
 ******************************************************************************/

#include <cmath>
#include <ctime>
#include <iostream>
#include <cstdlib>
#include "../inc/chCommandLine.h"
#include "../inc/chTimer.hpp"
#include <cstdio>
#include <iomanip>

#define GAMMA  6.673e-11f // (Nm^2)/(kg^2)
#define TIMESTEP  1e-4f // s
#define SOFTENING 1e-7f

const static int DEFAULT_NUM_ELEMENTS   = 1024;
const static int DEFAULT_NUM_ITERATIONS =  100;
const static int DEFAULT_BLOCK_DIM      =  128;




//
// Structures
//
// Use a SOA (Structure of Arrays)
//
struct Body_t {
    float4* posMass;  /* x = x */
                      /* y = y */
                      /* z = z */
                      /* w = Mass */
    float3* velocity; /* x = v_x*/
                      /* y = v_y */
                      /* z= v_z */

    Body_t() : posMass(NULL), velocity(NULL) {}
};

struct UP_Body_t {
    float* x, *y, *z;
    float* vx, *vy, *vz;
    float* mass;
};

//
// Function Prototypes
//
void printHelp(char *);
void printElement(UP_Body_t, int, int);

//
// Device Functions
//

//
// Calculate the Distance of two points
//
__device__ float
getDistance(float xA, float yA, float zA, float xB, float yB, float zB)
{
	float dx; float dy; float dz;

	dx = xB - xA;
	dy = yB - yA;
	dz = zB - zA;

	float distSquared = dx*dx + dy*dy + dz*dz;

	return sqrtf(distSquared);
}

//
// Calculate the forces between two bodies
//
__device__ void bodyBodyInteraction(float xA, float yA, float zA, float xB,
                                    float yB, float zB, float massB, float& Fx,
                                    float& Fy, float& Fz)
{
    xB -= xA;
    yB -= yA;
    zB -= zA;

	// TODO: Calc Acc
    float distSq = xB*xB + yB*yB + zB*zB;

	float InvSoftDist = rsqrtf(distSq + SOFTENING);
	float InvCubedSoftDist = InvSoftDist*InvSoftDist*InvSoftDist;

	float s = massB * InvCubedSoftDist; 

    //what we are adding up is actually the accelaration not the force since
    //later we want just the acceleration so we save us n multiplications and 1
    //division by massA
	Fx += xB * s;
	Fy += yB * s;
	Fz += zB * s;

}

//
// Calculate the new velocity of one particle
//
__device__ void calculateSpeed(float& vx, float& vy, float& vz, float Fx,
                               float Fy, float Fz)
{
    //dont care about leapfrog since starting the iteration gives me a headache
	vx += TIMESTEP * Fx;
	vy += TIMESTEP * Fy;
	vz += TIMESTEP * Fz;
}

//
// n-Body Kernel for the speed calculation
//
__global__ void simpleNbody_Kernel(int numElements, float* xPos, float* yPos,
                                   float* zPos, float* xVelo, float* yVelo,
                                   float* zVelo, float* mass)
{
    int elementId = blockIdx.x * blockDim.x + threadIdx.x;

	float x,y,z;
	float Fx, Fy, Fz;
	float vx, vy, vz;
	
	if (elementId < numElements) {
		x = xPos[elementId]; y = yPos[elementId]; z = zPos[elementId];
		vx = xVelo[elementId]; vy = yVelo[elementId]; vz = zVelo[elementId];
		Fx = 0.0; Fy = 0.0; Fz = 0.0;

        for (int i = 0; i < numElements; i++) {
            bodyBodyInteraction(x, y, z, xPos[i], yPos[i], zPos[i], mass[i],
                                Fx, Fy, Fz);
        }

        Fx = GAMMA * Fx;
		Fy = GAMMA * Fy;
		Fz = GAMMA * Fz;

		calculateSpeed(vx, vy, vz, Fx, Fy, Fz);

		//write new velocity to global memory
		xVelo[elementId] = vx;
		yVelo[elementId] = vy;
		zVelo[elementId] = vz;
	}
}

__global__ void
sharedNbody_Kernel(int numElements, float4* bodyPos, float3* bodySpeed)
{
	// Use the packed values and SOA to optimize load and store operations

	/*TODO Kernel Code*/
}

//
// n-Body Kernel to update the position
// Neended to prevent write-after-read-hazards
//
__global__ void updatePosition_Kernel(int numElements, float* xPos,
                                      float* yPos, float* zPos, float* xVelo,
                                      float* yVelo, float* zVelo)
{
    int elementId = blockIdx.x * blockDim.x + threadIdx.x;

	float x, y, z;
	float vx, vy, vz;

	if (elementId < numElements) {
		x = xPos[elementId]; y = yPos[elementId]; z = zPos[elementId]; 
		vx = xVelo[elementId]; vy = yVelo[elementId]; vz = zVelo[elementId];

		x += TIMESTEP * vx;
		y += TIMESTEP * vy;
		z += TIMESTEP * vz;

		xPos[elementId] = x;
		yPos[elementId] = y;
		zPos[elementId] = z;
	}
}

//
// Main
//
int
main(int argc, char * argv[])
{
	bool showHelp = chCommandLineGetBool("h", argc, argv);
	if (!showHelp) {
		showHelp = chCommandLineGetBool("help", argc, argv);
	}

	if (showHelp) {
		printHelp(argv[0]);
		exit(0);
	}

	std::cout << "***" << std::endl
			  << "*** Starting ..." << std::endl
			  << "***" << std::endl;

	ChTimer memCpyH2DTimer, memCpyD2HTimer;
	ChTimer kernelTimer;

	//
	// Allocate Memory
	//
	int numElements = 0;
	chCommandLineGet<int>(&numElements, "s", argc, argv);
	chCommandLineGet<int>(&numElements, "size", argc, argv);
	numElements = numElements != 0 ?
			numElements : DEFAULT_NUM_ELEMENTS;
	//
	// Host Memory
	//
	bool pinnedMemory = chCommandLineGetBool("p", argc, argv);
	if (!pinnedMemory) {
		pinnedMemory = chCommandLineGetBool("pinned-memory",argc,argv);
	}

	/*
	Body_t h_particles;
	*/
	UP_Body_t h_particles;

	if (!pinnedMemory) {
		// Pageable
		/*
		h_particles.posMass = static_cast<float4*>
				(malloc(static_cast<size_t>
				(numElements * sizeof(*(h_particles.posMass)))));
		h_particles.velocity = static_cast<float3*>
				(malloc(static_cast<size_t>
				(numElements * sizeof(*(h_particles.velocity)))));
				*/
		h_particles.x = static_cast<float*>
			(malloc(static_cast<size_t>
			(numElements * sizeof(*(h_particles.x)))));
		h_particles.y = static_cast<float*>
			(malloc(static_cast<size_t>
			(numElements * sizeof(*(h_particles.y)))));
		h_particles.z = static_cast<float*>
			(malloc(static_cast<size_t>
			(numElements * sizeof(*(h_particles.z)))));
		h_particles.vx = static_cast<float*>
			(malloc(static_cast<size_t>
			(numElements * sizeof(*(h_particles.vx)))));
		h_particles.vy = static_cast<float*>
			(malloc(static_cast<size_t>
			(numElements * sizeof(*(h_particles.vy)))));
		h_particles.vz = static_cast<float*>
			(malloc(static_cast<size_t>
			(numElements * sizeof(*(h_particles.vz)))));
		h_particles.mass = static_cast<float*>
			(malloc(static_cast<size_t>
			(numElements * sizeof(*(h_particles.mass)))));
	} else {
		// Pinned
		cudaMallocHost(&(h_particles.x),
			static_cast<size_t>
			(numElements * sizeof(*(h_particles.x))));
		cudaMallocHost(&(h_particles.y),
			static_cast<size_t>
			(numElements * sizeof(*(h_particles.y))));
		cudaMallocHost(&(h_particles.z),
			static_cast<size_t>
			(numElements * sizeof(*(h_particles.z))));
		cudaMallocHost(&(h_particles.vx),
			static_cast<size_t>
			(numElements * sizeof(*(h_particles.vx))));
		cudaMallocHost(&(h_particles.vy),
			static_cast<size_t>
			(numElements * sizeof(*(h_particles.vy))));
		cudaMallocHost(&(h_particles.vz),
			static_cast<size_t>
			(numElements * sizeof(*(h_particles.vz))));
		cudaMallocHost(&(h_particles.mass),
			static_cast<size_t>
			(numElements * sizeof(*(h_particles.mass))));
	}

	// Init Particles
//	srand(static_cast<unsigned>(time(0)));
	srand(0); // Always the same random numbers
	for (int i = 0; i < numElements; i++) {
		h_particles.x[i] = static_cast<float>( 1e-8*static_cast<float>(rand())); // Modify the random values to
		h_particles.y[i] = static_cast<float>( 1e-8*static_cast<float>(rand())); // increase the position changes
		h_particles.z[i] = static_cast<float>( 1e-8*static_cast<float>(rand())); // and the velocity
		h_particles.mass[i] = static_cast<float>( 1e4*static_cast<float>(rand()));
		h_particles.vx[i] = 0.0f;
		h_particles.vy[i] = 0.0f;
		h_particles.vz[i] = 0.0f;
	}
	
	printElement(h_particles, 0, 0);

	// Device Memory
	UP_Body_t d_particles;
	cudaMalloc(&(d_particles.x), 
			static_cast<size_t>(numElements * sizeof(*(d_particles.x))));
	cudaMalloc(&(d_particles.y),
		static_cast<size_t>(numElements * sizeof(*(d_particles.y))));
	cudaMalloc(&(d_particles.z),
		static_cast<size_t>(numElements * sizeof(*(d_particles.z))));
	cudaMalloc(&(d_particles.vx),
		static_cast<size_t>(numElements * sizeof(*(d_particles.vx))));
	cudaMalloc(&(d_particles.vy),
		static_cast<size_t>(numElements * sizeof(*(d_particles.vy))));
	cudaMalloc(&(d_particles.vz),
		static_cast<size_t>(numElements * sizeof(*(d_particles.vz))));
	cudaMalloc(&(d_particles.mass),
		static_cast<size_t>(numElements * sizeof(*(d_particles.mass))));

	if (h_particles.x == NULL || h_particles.y == NULL || h_particles.z == NULL || h_particles.vx == NULL || h_particles.vy == NULL || h_particles.vz == NULL || h_particles.mass == NULL ||
		d_particles.x == NULL || d_particles.y == NULL || d_particles.z == NULL || d_particles.vx == NULL || d_particles.vy == NULL || d_particles.vz == NULL || d_particles.mass == NULL) {
		std::cout << "\033[31m***" << std::endl
		          << "*** Error - Memory allocation failed" << std::endl
		          << "***\033[0m" << std::endl;

		exit(-1);
	}

	//
	// Copy Data to the Device
	//
	memCpyH2DTimer.start();

	cudaMemcpy(d_particles.x, h_particles.x, 
			static_cast<size_t>(numElements * sizeof(float)), 
			cudaMemcpyHostToDevice);
	cudaMemcpy(d_particles.y, h_particles.y,
		static_cast<size_t>(numElements * sizeof(float)),
		cudaMemcpyHostToDevice);
	cudaMemcpy(d_particles.z, h_particles.z,
		static_cast<size_t>(numElements * sizeof(float)),
		cudaMemcpyHostToDevice);
	cudaMemcpy(d_particles.vx, h_particles.vx,
		static_cast<size_t>(numElements * sizeof(float)),
		cudaMemcpyHostToDevice);
	cudaMemcpy(d_particles.vy, h_particles.vy,
		static_cast<size_t>(numElements * sizeof(float)),
		cudaMemcpyHostToDevice);
	cudaMemcpy(d_particles.vz, h_particles.vz,
		static_cast<size_t>(numElements * sizeof(float)),
		cudaMemcpyHostToDevice);
	cudaMemcpy(d_particles.mass, h_particles.mass,
		static_cast<size_t>(numElements * sizeof(float)),
		cudaMemcpyHostToDevice);

	memCpyH2DTimer.stop();

	//
	// Get Kernel Launch Parameters
	//
	int blockSize = 0,
	    gridSize = 0,
	    numIterations = 0;

	// Number of Iterations	
	chCommandLineGet<int>(&numIterations,"i", argc, argv);
	chCommandLineGet<int>(&numIterations,"num-iterations", argc, argv);
	numIterations = numIterations != 0 ? numIterations : DEFAULT_NUM_ITERATIONS;

	// Block Dimension / Threads per Block
	chCommandLineGet<int>(&blockSize,"t", argc, argv);
	chCommandLineGet<int>(&blockSize,"threads-per-block", argc, argv);
	blockSize = blockSize != 0 ? blockSize : DEFAULT_BLOCK_DIM;

	if (blockSize > 1024) {
		std::cout << "\033[31m***" << std::endl
		          << "*** Error - The number of threads per block is too big" << std::endl
		          << "***\033[0m" << std::endl;

		exit(-1);
	}

	gridSize = ceil(static_cast<float>(numElements) / static_cast<float>(blockSize));

	dim3 grid_dim = dim3(gridSize);
	dim3 block_dim = dim3(blockSize);

	std::cout << "***" << std::endl;
	std::cout << "*** Grid: " << gridSize << std::endl;
	std::cout << "*** Block: " << blockSize << std::endl;
	std::cout << "***" << std::endl;

	kernelTimer.start();

	for (int i = 0; i < numIterations; i ++) {
		
		simpleNbody_Kernel <<<grid_dim, block_dim >>>(numElements, d_particles.x, d_particles.y, d_particles.z, d_particles.vx, d_particles.vy, d_particles.vz, d_particles.mass);
		
		updatePosition_Kernel <<<grid_dim, block_dim >>>(numElements, d_particles.x, d_particles.y, d_particles.z, d_particles.vx, d_particles.vy, d_particles.vz);
		
/*
		cudaMemcpy(h_particles.x, d_particles.x, sizeof(float), cudaMemcpyDeviceToHost);
		cudaMemcpy(h_particles.y, d_particles.y, sizeof(float), cudaMemcpyDeviceToHost);
		cudaMemcpy(h_particles.z, d_particles.z, sizeof(float), cudaMemcpyDeviceToHost);
		cudaMemcpy(h_particles.vx, d_particles.vx, sizeof(float), cudaMemcpyDeviceToHost);
		cudaMemcpy(h_particles.vy, d_particles.vy, sizeof(float), cudaMemcpyDeviceToHost);
		cudaMemcpy(h_particles.vz, d_particles.vz, sizeof(float), cudaMemcpyDeviceToHost);
		cudaMemcpy(h_particles.mass, d_particles.mass, sizeof(float), cudaMemcpyDeviceToHost);

		printElement(h_particles, 0, i+1);
*/
    }
	
	// Synchronize
	cudaDeviceSynchronize();

	// Check for Errors
	cudaError_t cudaError = cudaGetLastError();
	if ( cudaError != cudaSuccess ) {
		std::cout << "\033[31m***" << std::endl
		          << "***ERROR*** " << cudaError << " - " << cudaGetErrorString(cudaError)
		          << std::endl
		          << "***\033[0m" << std::endl;

		return -1;
	}

	kernelTimer.stop();

	//
	// Copy Back Data
	//
	memCpyD2HTimer.start();
	
	cudaMemcpy(h_particles.x, d_particles.x, 
			static_cast<size_t>(numElements * sizeof(*(h_particles.x))), 
			cudaMemcpyDeviceToHost);
	cudaMemcpy(h_particles.y, d_particles.y,
		static_cast<size_t>(numElements * sizeof(*(h_particles.y))),
		cudaMemcpyDeviceToHost);
	cudaMemcpy(h_particles.z, d_particles.z,
		static_cast<size_t>(numElements * sizeof(*(h_particles.z))),
		cudaMemcpyDeviceToHost);
	cudaMemcpy(h_particles.vx, d_particles.vx,
		static_cast<size_t>(numElements * sizeof(*(h_particles.vx))),
		cudaMemcpyDeviceToHost);
	cudaMemcpy(h_particles.vy, d_particles.vy,
		static_cast<size_t>(numElements * sizeof(*(h_particles.vy))),
		cudaMemcpyDeviceToHost);
	cudaMemcpy(h_particles.vz, d_particles.vz,
		static_cast<size_t>(numElements * sizeof(*(h_particles.vz))),
		cudaMemcpyDeviceToHost);
	cudaMemcpy(h_particles.mass, d_particles.mass,
		static_cast<size_t>(numElements * sizeof(*(h_particles.mass))),
		cudaMemcpyDeviceToHost);

    printElement(h_particles, 0, numIterations);
	memCpyD2HTimer.stop();

	// Free Memory
	if (!pinnedMemory) {
		free(h_particles.x);
		free(h_particles.y);
		free(h_particles.z);
		free(h_particles.vx);
		free(h_particles.vy);
		free(h_particles.vz);
		free(h_particles.mass);
	} else {
		cudaFreeHost(h_particles.x);
		cudaFreeHost(h_particles.y);
		cudaFreeHost(h_particles.z);
		cudaFreeHost(h_particles.vx);
		cudaFreeHost(h_particles.vy);
		cudaFreeHost(h_particles.vz);
		cudaFreeHost(h_particles.mass);
	}

	cudaFree(d_particles.x);
	cudaFree(d_particles.y);
	cudaFree(d_particles.z);
	cudaFree(d_particles.vx);
	cudaFree(d_particles.vy);
	cudaFree(d_particles.vz);
	cudaFree(d_particles.mass);
	
	// Print Meassurement Results
    std::cout << "***" << std::endl
              << "*** Results:" << std::endl
              << "***    Num Elements: " << numElements << std::endl
              << "***    Time to Copy to Device: " << 1e3 * memCpyH2DTimer.getTime()
                << " ms" << std::endl
              << "***    Copy Bandwidth: " 
                << 1e-9 * memCpyH2DTimer.getBandwidth(numElements * sizeof(h_particles))
                << " GB/s" << std::endl
              << "***    Time to Copy from Device: " << 1e3 * memCpyD2HTimer.getTime()
                << " ms" << std::endl
              << "***    Copy Bandwidth: " 
                << 1e-9 * memCpyD2HTimer.getBandwidth(numElements * sizeof(h_particles))
                << " GB/s" << std::endl
              << "***    Time for n-Body Computation: " << 1e3 * kernelTimer.getTime()
                << " ms" << std::endl
              << "***" << std::endl;

	return 0;
}

void
printHelp(char * argv)
{
    std::cout << "Help:" << std::endl
              << "  Usage: " << std::endl
              << "  " << argv << " [-p] [-s <num-elements>] [-t <threads_per_block>]"
                  << std::endl
              << "" << std::endl
              << "  -p|--pinned-memory" << std::endl
              << "    Use pinned Memory instead of pageable memory" << std::endl
              << "" << std::endl
              << "  -s <num-elements>|--size <num-elements>" << std::endl
              << "    Number of elements (particles)" << std::endl
              << "" << std::endl
              << "  -t <threads_per_block>|--threads-per-block <threads_per_block>" 
                  << std::endl
              << "    The number of threads per block" << std::endl
              << "" << std::endl;
}

//
// Print one element
//

void
printElement(UP_Body_t particles, int elementId, int iteration)
{

    std::cout << "***" << std::endl
              << "*** Printing Element " << elementId << " in iteration " << iteration << std::endl
              << "***" << std::endl
              << "*** Position: <" 
			  << std::setw(11) << std::setprecision(9) << particles.x[elementId] << "|"
			  << std::setw(11) << std::setprecision(9) << particles.y[elementId] << "|"
			  << std::setw(11) << std::setprecision(9) << particles.z[elementId] << "> [m]" << std::endl
              << "*** velocity: <" 
			  << std::setw(11) << std::setprecision(9) << particles.vx[elementId] << "|"
			  << std::setw(11) << std::setprecision(9) << particles.vy[elementId] << "|"
			  << std::setw(11) << std::setprecision(9) << particles.vz[elementId] << "> [m/s]" << std::endl
              << "*** Mass: " 
			  << std::setw(11) << std::setprecision(9) << particles.mass[elementId] << " kg" << std::endl
              << "***" << std::endl;
}
