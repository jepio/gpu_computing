import re
import sys

bodies_pattern = re.compile(r"Num Elements: (\d+)")
time_pattern = re.compile(r"Time for n-Body Computation: (\d+\.\d+) ms")

filename = sys.argv[1]
file_ = open(filename)
lines = file_.read()

bodies = (int(l.group(1)) for l in re.finditer(bodies_pattern, lines))
time = (float(l.group(1)) for l in re.finditer(time_pattern, lines))

results = zip(bodies, time)

print "#bodies time(ms)"
for i, j in results:
    print i, j
