import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

plt.rc("font", family="sans-serif")
plt.rc("font", size=12, serif="Helvetica")
plt.rc("legend", fontsize=11)
plt.rc("text", usetex=True)

def linear(x, a, b):
    return a*x + b

def parabolic(x, a, b, c):
    return a*(x - b) ** 2 + c

x, y = np.genfromtxt("a1.col", unpack=True)
xx = np.linspace(np.min(x), np.max(x))
xxx = np.linspace(8000, np.max(x))
cut = 8

linear_pars, _ =  curve_fit(linear, x[2:cut], y[2:cut])
parabolic_pars, _ = curve_fit(parabolic, x[cut+1:], y[cut+1:])

scale = 1.3
plt.figure(figsize=(5/scale, 4/scale))
plt.loglog(x, y, "o", markersize=5, label="Measurement")
plt.plot(xx, linear(xx, *linear_pars), "--", label="Linear")
plt.plot(xxx, parabolic(xxx, *parabolic_pars), "--", label="Quadratic")
plt.legend(loc=0, frameon=False)
plt.xlabel("Number of elements")
plt.ylabel("Time / ms")
plt.title("Computation time - naive GPU")

plt.tight_layout()
plt.savefig("a1.eps")
