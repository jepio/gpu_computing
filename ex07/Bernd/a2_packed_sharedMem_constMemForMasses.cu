/******************************************************************************
 *
 *           XXXII Heidelberg Physics Graduate Days - GPU Computing
 *
 *                 Gruppe : TODO
 *
 *                   File : main.cu
 *
 *                Purpose : n-Body Computation
 *
 ******************************************************************************/

#include <cmath>
#include <ctime>
#include <iostream>
#include <cstdlib>
#include "../inc/chCommandLine.h"
#include "../inc/chTimer.hpp"
#include <cstdio>
#include <iomanip>

#define GAMMA  6.673e-11 // (Nm^2)/(kg^2)
#define TIMESTEP  1e-4 // s

const static int DEFAULT_NUM_ELEMENTS   = 1024;
const static int DEFAULT_NUM_ITERATIONS =  100;
const static int DEFAULT_BLOCK_DIM      =  128;


//
// Structures
//
// Use a SOA (Structure of Arrays)
//
struct Body_t {
	float3* position;  /* x = x */
	                  /* y = y */
	                  /* z = z */
	                  /* w = Mass */
	float3* velocity; /* x = v_x*/
	                  /* y = v_y */
	                  /* z= v_z */
	float*	mass;
	
	Body_t(): position(NULL), velocity(NULL), mass(NULL) {}
	};

//
// Function Prototypes
//
void printHelp(char *);
void printElement(Body_t, int, int);

//
// Device Functions
//

// try out constant memory ... no way to dynamically allocate so take all and use as needed... only the mass of the particles will ga there as they dont change over run time

__constant__ float constMemMass[16384];

//
// Calculate the Distance of two points
//
__device__ float
getDistance(float3 a, float3 b)
{
	float dx; float dy; float dz;

	dx = b.x - a.x;
	dy = b.y - a.y;
	dz = b.z - a.z;

	float distSquared = dx*dx + dy*dy + dz*dz;

	return sqrtf(distSquared);
}

//
// Calculate the forces between two bodies
//
__device__ void
bodyBodyInteraction(float3 bodyA, float3 bodyB, float3& force, int whatElement)
{
	float distance = getDistance(bodyA, bodyB);

	// TODO: Calc Acc

	float InvSoftDist = rsqrtf(distance*distance + 0.001);
	float InvCubedSoftDist = InvSoftDist*InvSoftDist*InvSoftDist;

	float s = constMemMass[whatElement] * InvCubedSoftDist;

	//what we are adding up is actually the accelaration not the force since later we want just the acceleration so we save us n multiplications and 1 division by massA
	force.x += (bodyB.x - bodyA.x) * s;
	force.y += (bodyB.y - bodyA.y) * s;
	force.z += (bodyB.z - bodyA.z) * s;
}

//
// Calculate the new velocity of one particle
//
__device__ void
calculateSpeed(float3& currentSpeed, float3 force)
{
	//dont care about leapfrog since starting the iteration gives me a headache
	currentSpeed.x += TIMESTEP * force.x;
	currentSpeed.y += TIMESTEP * force.y;
	currentSpeed.z += TIMESTEP * force.z;
}

//
// n-Body Kernel for the speed calculation
//
/*
__global__ void
simpleNbody_Kernel(int numElements, float4* bodyPos, float3* bodySpeed)
{
	int elementId = blockIdx.x * blockDim.x + threadIdx.x;

	float4 elementPosMass;
	float3 elementForce;
	float3 elementSpeed;
	
	if (elementId < numElements) {
		elementPosMass = bodyPos[elementId];
		elementSpeed = bodySpeed[elementId];
		elementForce = make_float3(0,0,0);

		for (int i = 0; i < numElements; i++) {
			if (i != elementId) {
				bodyBodyInteraction(elementPosMass, bodyPos[i], elementForce);
			}
		}

		elementForce.x = GAMMA * elementForce.x;
		elementForce.y = GAMMA * elementForce.y;
		elementForce.z = GAMMA * elementForce.z;

		calculateSpeed(elementSpeed, elementForce);

		bodySpeed[elementId] = elementSpeed;
	}
}
*/

__global__ void
sharedNbody_Kernel(int numElements, float3* bodyPos, float3* bodySpeed)
{
	extern __shared__ float3 sh_bodyPos[]; 


	//int elementId = blockIdx.x * blockDim.x + threadIdx.x;

	for (int i = blockIdx.x*blockDim.x + threadIdx.x;
		i < numElements; i += blockDim.x*gridDim.x)
	{
		float3 elementPos = bodyPos[i];
		float3 elementForce = make_float3(0, 0, 0);

		for (int j = 0; j < numElements; j += blockDim.x)
		{
			sh_bodyPos[threadIdx.x] = bodyPos[j+threadIdx.x];
			__syncthreads();

			for (int k = 0; k < blockDim.x; k++)
			{
				float3 bodyPosition = sh_bodyPos[k];

				//if (k != i) {
					bodyBodyInteraction(elementPos, bodyPosition, elementForce, j+k);
				//}
			}
			__syncthreads();
		}
		float3 elementSpeed = bodySpeed[i];
		elementForce.x = GAMMA * elementForce.x;
		elementForce.y = GAMMA * elementForce.y;
		elementForce.z = GAMMA * elementForce.z;

		calculateSpeed(elementSpeed, elementForce);

		bodySpeed[i] = elementSpeed;

	}

	/*
	float3 elementPos;
	float3 elementForce;
	float3 elementSpeed;
	

	if (elementId < numElements) {
		
		elementPos = bodyPos[elementId];
		elementSpeed = bodySpeed[elementId];
		elementForce = make_float3(0, 0, 0);
		
		for (int i = 0; i < numElements; i++) 
		{

			if (i != elementId) {
				bodyBodyInteraction(elementPos, bodyPos[i], elementForce, i);
			}
		}

		elementForce.x = GAMMA * elementForce.x;
		elementForce.y = GAMMA * elementForce.y;
		elementForce.z = GAMMA * elementForce.z;

		calculateSpeed(elementSpeed, elementForce);

		bodySpeed[elementId] = elementSpeed;
	}
	*/
}

//
// n-Body Kernel to update the position
// Neended to prevent write-after-read-hazards
//
__global__ void
updatePosition_Kernel(int numElements, float3* bodyPos, float3* bodySpeed)
{
	int elementId = blockIdx.x * blockDim.x + threadIdx.x;

	float3 elementPos;
	float3 elementSpeed;

	if (elementId < numElements) {
		elementPos = bodyPos[elementId];
		elementSpeed = bodySpeed[elementId];

		elementPos.x += TIMESTEP * elementSpeed.x;
		elementPos.y += TIMESTEP * elementSpeed.y;
		elementPos.z += TIMESTEP * elementSpeed.z;

		bodyPos[elementId] = elementPos;
	}
}

//
// Main
//
int
main(int argc, char * argv[])
{
	bool showHelp = chCommandLineGetBool("h", argc, argv);
	if (!showHelp) {
		showHelp = chCommandLineGetBool("help", argc, argv);
	}

	if (showHelp) {
		printHelp(argv[0]);
		exit(0);
	}

	std::cout << "***" << std::endl
			  << "*** Starting ..." << std::endl
			  << "***" << std::endl;

	ChTimer memCpyH2DTimer, memCpyD2HTimer;
	ChTimer kernelTimer;

	//
	// Allocate Memory
	//
	int numElements = 0;
	chCommandLineGet<int>(&numElements, "s", argc, argv);
	chCommandLineGet<int>(&numElements, "size", argc, argv);
	numElements = numElements != 0 ?
	numElements : DEFAULT_NUM_ELEMENTS;

	if (numElements > 16384) {
		std::cout << "\033[31m***" << std::endl
			<< "*** Error - Too many bodies... not enough constant memory" << std::endl
			<< "***\033[0m" << std::endl;

		exit(-1);
	}

	//
	// Host Memory
	//
	bool pinnedMemory = chCommandLineGetBool("p", argc, argv);
	if (!pinnedMemory) {
		pinnedMemory = chCommandLineGetBool("pinned-memory",argc,argv);
	}

	Body_t h_particles;
	if (!pinnedMemory) {
		// Pageable
		h_particles.position = static_cast<float3*>
				(malloc(static_cast<size_t>
				(numElements * sizeof(*(h_particles.position)))));
		h_particles.velocity = static_cast<float3*>
				(malloc(static_cast<size_t>
				(numElements * sizeof(*(h_particles.velocity)))));
		h_particles.mass = static_cast<float*>
			(malloc(static_cast<size_t>
			(numElements * sizeof(*(h_particles.mass)))));
	} else {
		// Pinned
		cudaMallocHost(&(h_particles.position),
				static_cast<size_t>
				(numElements * sizeof(*(h_particles.position))));
		cudaMallocHost(&(h_particles.velocity), 
				static_cast<size_t>
				(numElements * sizeof(*(h_particles.velocity))));
		cudaMallocHost(&(h_particles.mass),
			static_cast<size_t>
			(numElements * sizeof(*(h_particles.mass))));
	}

	// Init Particles
//	srand(static_cast<unsigned>(time(0)));
	srand(0); // Always the same random numbers
	for (int i = 0; i < numElements; i++) {
		h_particles.position[i].x = 1e-8*static_cast<float>(rand()); // Modify the random values to
		h_particles.position[i].y = 1e-8*static_cast<float>(rand()); // increase the position changes
		h_particles.position[i].z = 1e-8*static_cast<float>(rand()); // and the velocity
		h_particles.mass[i] =  1e4*static_cast<float>(rand());
		h_particles.velocity[i].x = 0.0f;
		h_particles.velocity[i].y = 0.0f;
		h_particles.velocity[i].z = 0.0f;
	}
	
	printElement(h_particles, 0, 0);

	// Device Memory
	Body_t d_particles;
	cudaMalloc(&(d_particles.position),
		static_cast<size_t>(numElements * sizeof(*(d_particles.position))));
	cudaMalloc(&(d_particles.velocity), 
			static_cast<size_t>(numElements * sizeof(*(d_particles.velocity))));

	if (h_particles.position == NULL || h_particles.velocity == NULL || h_particles.mass == NULL ||
		d_particles.position == NULL || d_particles.velocity == NULL) {
		std::cout << "\033[31m***" << std::endl
		          << "*** Error - Memory allocation failed" << std::endl
		          << "***\033[0m" << std::endl;

		exit(-1);
	}

	//
	// Copy Data to the Device
	//
	memCpyH2DTimer.start();

	cudaMemcpy(d_particles.position, h_particles.position,
			static_cast<size_t>(numElements * sizeof(float3)), 
			cudaMemcpyHostToDevice);
	cudaMemcpy(d_particles.velocity, h_particles.velocity, 
			static_cast<size_t>(numElements * sizeof(float3)), 
			cudaMemcpyHostToDevice);

	//special treatment for the mass

	cudaMemcpyToSymbol(constMemMass, h_particles.mass, sizeof(float)* numElements);

	memCpyH2DTimer.stop();

	//
	// Get Kernel Launch Parameters
	//
	int blockSize = 0,
	    gridSize = 0,
	    numIterations = 0;

	// Number of Iterations	
	chCommandLineGet<int>(&numIterations,"i", argc, argv);
	chCommandLineGet<int>(&numIterations,"num-iterations", argc, argv);
	numIterations = numIterations != 0 ? numIterations : DEFAULT_NUM_ITERATIONS;

	// Block Dimension / Threads per Block
	chCommandLineGet<int>(&blockSize,"t", argc, argv);
	chCommandLineGet<int>(&blockSize,"threads-per-block", argc, argv);
	blockSize = blockSize != 0 ? blockSize : DEFAULT_BLOCK_DIM;

	if (blockSize > 1024) {
		std::cout << "\033[31m***" << std::endl
		          << "*** Error - The number of threads per block is too big" << std::endl
		          << "***\033[0m" << std::endl;

		exit(-1);
	}

	gridSize = ceil(static_cast<float>(numElements) / static_cast<float>(blockSize));

	dim3 grid_dim = dim3(gridSize);
	dim3 block_dim = dim3(blockSize);

	std::cout << "***" << std::endl;
	std::cout << "*** Grid: " << gridSize << std::endl;
	std::cout << "*** Block: " << blockSize << std::endl;
	std::cout << "***" << std::endl;

	int sharedMemSize = 3 * blockSize * sizeof(float);

	kernelTimer.start();

	for (int i = 0; i < numIterations; i ++) {
		sharedNbody_Kernel <<<grid_dim, block_dim, sharedMemSize >>>(numElements, d_particles.position, d_particles.velocity);

		updatePosition_Kernel <<<grid_dim, block_dim >>>(numElements, d_particles.position, d_particles.velocity);
/*
		cudaMemcpy(h_particles.position, d_particles.position, sizeof(float3), cudaMemcpyDeviceToHost);
		cudaMemcpy(h_particles.velocity, d_particles.velocity, sizeof(float3), cudaMemcpyDeviceToHost);
*/
	}
	
	// Synchronize
	cudaDeviceSynchronize();

	// Check for Errors
	cudaError_t cudaError = cudaGetLastError();
	if ( cudaError != cudaSuccess ) {
		std::cout << "\033[31m***" << std::endl
		          << "***ERROR*** " << cudaError << " - " << cudaGetErrorString(cudaError)
		          << std::endl
		          << "***\033[0m" << std::endl;

		return -1;
	}

	kernelTimer.stop();

	//
	// Copy Back Data
	//
	memCpyD2HTimer.start();
	
	cudaMemcpy(h_particles.position, d_particles.position,
		static_cast<size_t>(numElements * sizeof(*(h_particles.position))),
			cudaMemcpyDeviceToHost);
	cudaMemcpy(h_particles.velocity, d_particles.velocity, 
			static_cast<size_t>(numElements * sizeof(*(h_particles.velocity))), 
			cudaMemcpyDeviceToHost);

	memCpyD2HTimer.stop();
	printElement(h_particles, 0, numIterations);

	// Free Memory
	if (!pinnedMemory) {
		free(h_particles.position);
		free(h_particles.velocity);
		free(h_particles.mass);
	} else {
		cudaFreeHost(h_particles.position);
		cudaFreeHost(h_particles.velocity);
	}

	cudaFree(d_particles.position);
	cudaFree(d_particles.velocity);
	
	// Print Meassurement Results
    std::cout << "***" << std::endl
              << "*** Results:" << std::endl
              << "***    Num Elements: " << numElements << std::endl
              << "***    Time to Copy to Device: " << 1e3 * memCpyH2DTimer.getTime()
                << " ms" << std::endl
              << "***    Copy Bandwidth: " 
                << 1e-9 * memCpyH2DTimer.getBandwidth(numElements * sizeof(h_particles))
                << " GB/s" << std::endl
              << "***    Time to Copy from Device: " << 1e3 * memCpyD2HTimer.getTime()
                << " ms" << std::endl
              << "***    Copy Bandwidth: " 
                << 1e-9 * memCpyD2HTimer.getBandwidth(numElements * sizeof(h_particles))
                << " GB/s" << std::endl
              << "***    Time for n-Body Computation: " << 1e3 * kernelTimer.getTime()
                << " ms" << std::endl
              << "***" << std::endl;

	return 0;
}

void
printHelp(char * argv)
{
    std::cout << "Help:" << std::endl
              << "  Usage: " << std::endl
              << "  " << argv << " [-p] [-s <num-elements>] [-t <threads_per_block>]"
                  << std::endl
              << "" << std::endl
              << "  -p|--pinned-memory" << std::endl
              << "    Use pinned Memory instead of pageable memory" << std::endl
              << "" << std::endl
              << "  -s <num-elements>|--size <num-elements>" << std::endl
              << "    Number of elements (particles)" << std::endl
              << "" << std::endl
              << "  -t <threads_per_block>|--threads-per-block <threads_per_block>" 
                  << std::endl
              << "    The number of threads per block" << std::endl
              << "" << std::endl;
}

//
// Print one element
//
void
printElement(Body_t particles, int elementId, int iteration)
{
    float3 pos = particles.position[elementId];
    float3 velocity = particles.velocity[elementId];
	float  mass = particles.mass[elementId];

    std::cout << "***" << std::endl
              << "*** Printing Element " << elementId << " in iteration " << iteration << std::endl
              << "***" << std::endl
              << "*** Position: <" 
				  << std::setw(11) << std::setprecision(9) << pos.x << "|"
				  << std::setw(11) << std::setprecision(9) << pos.y << "|"
				  << std::setw(11) << std::setprecision(9) << pos.z << "> [m]" << std::endl
			  << "*** velocity: <" 
                  << std::setw(11) << std::setprecision(9) << velocity.x << "|"
                  << std::setw(11) << std::setprecision(9) << velocity.y << "|"
                  << std::setw(11) << std::setprecision(9) << velocity.z << "> [m/s]" << std::endl
              << "*** Mass: " 
                  << std::setw(11) << std::setprecision(9) << mass << " kg"<< std::endl
              << "***" << std::endl;
}
