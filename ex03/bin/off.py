import sys

if len(sys.argv) < 2:
    print "Launch with: python format.py filename"
    sys.exit(1)

name = sys.argv[1]
file_ = open(name)

lines = file_.readlines()
right_lines = lines[9::10]
b = [f.strip().split('(') for f in right_lines]
b = [[int(l[1].split(")")[0]), float(l[1].split("bw=")[-1][:-4])] for l in b]

for i, j in b:
    print i, j
