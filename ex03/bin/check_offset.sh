#!/usr/bin/env bash
#SBATCH --gres=gpu
#SBATCH -o offset

OFFSETS=$(seq 0 4 128)

for off in $OFFSETS
do
    ./main --global-offset --offset $off -t 512 -g 2048 -i 10000
done
