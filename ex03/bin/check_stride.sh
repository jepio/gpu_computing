#!/usr/bin/env bash
#SBATCH --gres=gpu
#SBATCH -o STRIDE

STRIDE=$(seq 1 4 128)

for str in $STRIDE
do
    ./main --global-stride --stride $str -t 512 -g 1024 -i 10000
done
