/******************************************************************************
 *
 *Computer Engineering Group, Heidelberg University - GPU Computing Exercise 04
 *
 *                  Group : TBD
 *
 *                   File : main.cu
 *
 *                Purpose : Memory Operations Benchmark
 *
 ******************************************************************************/

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <chCommandLine.h>
#include <chTimer.hpp>
#include <stdio.h>
#include <assert.h>

const static int DEFAULT_MEM_SIZE       = 10*1024*1024; // 10 MB
const static int DEFAULT_NUM_ITERATIONS =         1000;
const static int DEFAULT_BLOCK_DIM      =          128;
const static int DEFAULT_GRID_DIM       =           16;

//
// Function Prototypes
//
void printHelp(char *);

//
// Test Kernel
//
#define MAX_SHARED ((48<<10) / sizeof(float))

__global__ void
globalMem2SharedMem(const float *in, int n)
{
	// max shared mem = 48 kB / sizeof(int)
	__shared__ float s[MAX_SHARED];
	int tid = threadIdx.x;

	int offset = blockDim.x;

	while (tid < n) {
		s[tid] = in[tid];
		tid += offset;
	}
}

__global__ void
SharedMem2globalMem(float *out, int n)
{
	// max shared mem = 48 kB / sizeof(int)
	__shared__ float s[MAX_SHARED];
	int tid = threadIdx.x;

	int offset = blockDim.x;

	while (tid < n) {
		out[tid] = s[tid];
		tid += offset;
	}
}


__global__ void
SharedMem2Registers(float *var, int n)
{
	extern __shared__ float s[];
	int tid = threadIdx.x;
	int offset = blockDim.x;
	// dummy variable
	float var_r;

	while (tid < n) {
		var_r = s[tid];
		tid += offset;
	}

	// conditional code that shouldnt get executed
	if (tid == 0)
		*var = var_r;
}

// Using these functions one can achieve the 1TB/s bandwidth of shared
// mem.
template<int N> __device__ __inline__
void copy(float *s, int t)
{
	copy<N - 1>(s, t);
	s[t + N] = t;
}

template<> __device__ __inline__
void copy<0>(float *s, int t)
{
	s[t] = t;
}

//template<int N>
__global__ void
Registers2SharedMem(float *var, int n)
{
	extern __shared__ float s[];
	int tid = threadIdx.x;
	int offset = blockDim.x;

	while (tid < n) {
		//copy<N>(s, tid);
		s[tid] = tid;
		tid += offset;
	}

	// conditional code that shouldnt get executed
	if (tid == 0)
		*var = s[tid];
}

/*use from cmd-line as follows
example:  	exercise04 --shared2register_conflict -s 4096 -t 1024 -g 1 -i 10 -stride 1

vary stride from 1 to 64

 */
#define SHARED_MEM_SIZE 1024 // 1Kb -> /4 -> number of bytes (float)
#define NUM_IN_KERNEL_ITERATIONS 100

__global__ void
bankConflictsRead
(long long * d_clocks, float * d_out, int iterations, int stride, int n)
{
	extern __shared__ float temp[];

	int tid = threadIdx.x; //+ blockIdx.x * blockDim.x; // not neccessary for ex 4_2
	int offset = gridDim.x * blockDim.x;

	while (tid < n) // make sure all data is initialized (will mostly on 1024 floats)
	{
		temp[tid] = (float)tid;			//initialize it first... otherwise compiler will optimize the shit out of it. Should be fast enough compared to mem accesses
		tid = tid + offset;
	}

	tid = threadIdx.x;			//reset tid otherwise no mem accesses will take place
	float myRegister = 1.0;

	long time_start = clock64();
	for (int j = 0; j < iterations; j++)
	{
		if (tid * stride < n)
		{
			myRegister = temp[tid * stride];
		}
	}

	d_clocks[0] = clock64() - time_start;

	if (tid == 1)
	{
		d_out[0] = myRegister;
		//printf("My register is %.2f \n", myRegister);
	}
}

//
// Main
//
int main ( int argc, char * argv[] )
{
	// Show Help
	bool optShowHelp = chCommandLineGetBool("h", argc, argv);
	if ( !optShowHelp )
		optShowHelp = chCommandLineGetBool("help", argc, argv);

	if ( optShowHelp ) {
		printHelp ( argv[0] );
		exit (0);
	}

	std::cout << "***" << std::endl
		<< "*** Starting ..." << std::endl
		<< "***" << std::endl;

	ChTimer kernelTimer;

	//
	// Get kernel launch parameters and configuration
	//
	int optNumIterations = 0,
	    optBlockSize = 0,
	    optGridSize = 0;

	// Number of Iterations
	chCommandLineGet<int> ( &optNumIterations,"i", argc, argv );
	chCommandLineGet<int> ( &optNumIterations,"iterations", argc, argv );
	optNumIterations = ( optNumIterations != 0 ) ? optNumIterations : DEFAULT_NUM_ITERATIONS;

	// Block Dimension / Threads per Block
	chCommandLineGet <int> ( &optBlockSize,"t", argc, argv );
	chCommandLineGet <int> ( &optBlockSize,"threads-per-block", argc, argv );
	optBlockSize = optBlockSize != 0 ? optBlockSize : DEFAULT_BLOCK_DIM;

	if ( optBlockSize > 1024 ) {
		std::cout << "\033[31m***" << std::cout
			<< "*** Error - The number of threads per block is too big"
			<< std::endl
			<< "***\033[0m" << std::endl;

		exit(-1);
	}

	// Grid Dimension
	chCommandLineGet <int> ( &optGridSize,"g", argc, argv );
	chCommandLineGet <int> ( &optGridSize,"grid-dim", argc, argv );
	optGridSize = optGridSize != 0 ? optGridSize : DEFAULT_GRID_DIM;

	dim3 grid_dim = dim3 ( optGridSize );
	dim3 block_dim = dim3 ( optBlockSize );


	int optModulo = 32*1024; // modulo in access pattern for conflict test
	chCommandLineGet <int> ( &optModulo,"mod", argc, argv );

	int optStride = 1; // modulo in access pattern for conflict test
	chCommandLineGet <int> ( &optStride,"stride", argc, argv );

	// Memory size
	int optMemorySize = 0;

	chCommandLineGet <int> ( &optMemorySize, "s", argc, argv );
	chCommandLineGet <int> ( &optMemorySize, "size", argc, argv );
	optMemorySize = optMemorySize != 0 ? optMemorySize : DEFAULT_MEM_SIZE;

	int numOfElementsInArray = optMemorySize / sizeof(float); 
	//
	// Device Memory
	//
	float* d_memoryA = NULL;
	cudaMalloc ( &d_memoryA, static_cast <size_t> ( optMemorySize ) ); // optMemorySize is in bytes

	float *outFloat = NULL;  // dummy variable to prevent compiler optimizations
	cudaMalloc ( &outFloat, static_cast <float> ( sizeof ( float ) ) );

	long hClocks = 0;
	long long *dClocks = NULL;
	cudaMalloc ( &dClocks, sizeof ( *dClocks ) );

	if ( d_memoryA == NULL || dClocks == NULL )
	{
		std::cout << "\033[31m***" << std::endl
			<< "*** Error - Memory allocation failed" << std::endl
			<< "***\033[0m" << std::endl;

		exit (-1);
	}

	//
	// Tests
	//
	long long averageClocks = 0; // if optNumIterations is too big might overflow!

	std::cout << "Starting kernel: " << grid_dim.x << "x" << block_dim.x << " threads, " << optMemorySize << "B shared memory" << ", " << optNumIterations << " iterations" << std::endl;
	kernelTimer.start();
	for ( int i = 0; i < optNumIterations; i++ )
	{
		//
		// Launch Kernel
		//
		if ( chCommandLineGetBool ( "global2shared", argc, argv ) )
		{
			globalMem2SharedMem <<< grid_dim, block_dim>>>
				(d_memoryA, optMemorySize / sizeof(float));
		}
		else if ( chCommandLineGetBool ( "shared2global", argc, argv ) )
		{
			SharedMem2globalMem <<< grid_dim, block_dim>>>
				(d_memoryA, optMemorySize / sizeof(float));
		}
		else if ( chCommandLineGetBool ( "shared2register", argc, argv ) )
		{
			SharedMem2Registers <<< grid_dim, block_dim, optMemorySize>>>
				(outFloat, optMemorySize / sizeof(float));
		}
		else if ( chCommandLineGetBool ( "register2shared", argc, argv ) )
		{
			Registers2SharedMem<<< grid_dim, block_dim, optMemorySize>>>
				(outFloat, optMemorySize / sizeof(float));
		}
		else if ( chCommandLineGetBool ( "shared2register_conflict", argc, argv ) )
		{
			bankConflictsRead << < grid_dim, block_dim, optMemorySize >> >
				(dClocks, outFloat, NUM_IN_KERNEL_ITERATIONS, optStride, numOfElementsInArray);

			if (chCommandLineGetBool("shared2register_conflict", argc, argv))
			{
				cudaError_t error = cudaMemcpy(&hClocks, dClocks, sizeof (long), cudaMemcpyDeviceToHost);
				if (error != cudaSuccess) {
					fprintf(stderr, "cudaMemcpy failed: %s\n", cudaGetErrorString(error));
					return 1;
				}
			}
			averageClocks += hClocks;
		}
	}

	// Mandatory synchronize after all kernel launches
	cudaDeviceSynchronize();
	kernelTimer.stop();

	cudaError_t cudaError = cudaGetLastError();
	if ( cudaError != cudaSuccess )
	{
		std::cout << "\033[31m***" << std::endl
			<< "***ERROR*** " << cudaError << " - " << cudaGetErrorString(cudaError)
			<< std::endl
			<< "***\033[0m" << std::endl;

		return -1;
	}

	// Print Measurement Results

	if ( chCommandLineGetBool ( "global2shared", argc, argv ) ) {
		std::cout << "Copy global->shared, size=" << std::setw(10) << optMemorySize * grid_dim.x << ", gDim=" << std::setw(5) << grid_dim.x << ", bDim=" << std::setw(5) << block_dim.x;
		//std::cout << ", time=" << kernelTimer.getTime(optNumIterations) <<
		std::cout.precision ( 2 );
		std::cout << ", bw=" << std::fixed << std::setw(6) << ( optMemorySize * grid_dim.x ) / kernelTimer.getTime(optNumIterations) / (1E09) << "GB/s" << std::endl;
	}

	if ( chCommandLineGetBool ( "shared2global", argc, argv ) ) {
		std::cout << "Copy shared->global, size=" << std::setw(10) << optMemorySize * grid_dim.x << ", gDim=" << std::setw(5) << grid_dim.x << ", bDim=" << std::setw(5) << block_dim.x;
		//std::cout << ", time=" << kernelTimer.getTime(optNumIterations) <<
		std::cout.precision ( 2 );
		std::cout << ", bw=" << std::fixed << std::setw(6) << ( optMemorySize * grid_dim.x ) / kernelTimer.getTime(optNumIterations) / (1E09) << "GB/s" << std::endl;
	}

	if ( chCommandLineGetBool ( "shared2register", argc, argv ) ) {
		std::cout << "Copy shared->register, size=" << std::setw(10) << optMemorySize << ", gDim=" << std::setw(5) << grid_dim.x << ", bDim=" << std::setw(5) << block_dim.x;
		//std::cout << ", time=" << kernelTimer.getTime(optNumIterations) <<
		std::cout.precision ( 2 );
		std::cout << ", bw=" << std::fixed << std::setw(6) << ( optMemorySize * grid_dim.x ) / kernelTimer.getTime(optNumIterations) / (1E09) << "GB/s" << std::endl;
	}

	if ( chCommandLineGetBool ( "register2shared", argc, argv ) ) {
		std::cout << "Copy register->shared, size=" << std::setw(10) << optMemorySize << ", gDim=" << std::setw(5) << grid_dim.x << ", bDim=" << std::setw(5) << block_dim.x;
		//std::cout << ", time=" << kernelTimer.getTime(optNumIterations) <<
		std::cout.precision ( 2 );
		std::cout << ", bw=" << std::fixed << std::setw(6) << (optMemorySize * grid_dim.x ) / kernelTimer.getTime(optNumIterations) / (1E09) << "GB/s" << std::endl;
	}

	if ( chCommandLineGetBool ( "shared2register_conflict", argc, argv ) ) {
		if ( chCommandLineGetBool ( "shared2register_conflict", argc, argv ) ) {
			cudaError_t error = cudaMemcpy ( &hClocks, dClocks, sizeof ( long ), cudaMemcpyDeviceToHost );
			if ( error != cudaSuccess) {
				fprintf ( stderr, "cudaMemcpy failed: %s\n", cudaGetErrorString ( error ) );
				return 1;
			}
		}

		std::cout << "Shared memory bank conflict test, size=" << std::setw(10) << optMemorySize << ", gDim=" << std::setw(5) << grid_dim.x << ", bDim=" << std::setw(5) << block_dim.x;
		std::cout << ", stride=" << std::setw(6) << optStride << ", modulo=" << std::setw(6) << optModulo;
		std::cout << ", clocks=" << std::setw(10) << (averageClocks / (float)optNumIterations) << std::endl;
		std::cout << ", time=" << std::setw(10) << kernelTimer.getTime(optNumIterations) << std::endl;
	}

	return 0;
}

void
printHelp(char * programName)
{
	std::cout
		<< "Usage: " << std::endl
		<< "  " << programName << " [-p] [-s <memory_size>] [-i <num_iterations>]" << std::endl
		<< "                [-t <threads_per_block>] [-g <blocks_per_grid]" << std::endl
		<< "                [-stride <stride>] [-offset <offset>]" << std::endl
		<< "  --{global2shared,shared2global,shared2register,register2shared,shared2register_conflict}" << std::endl
		<< "    Run kernel analyzing shared memory performance" << std::endl
		<< "  -s <memory_size>|--size <memory_size>" << std::endl
		<< "    The amount of memory to allcate" << std::endl
		<< "  -t <threads_per_block>|--threads-per-block <threads_per_block>" << std::endl
		<< "    The number of threads per block" << std::endl
		<< "  -g <blocks_per_grid>|--grid-dim <blocks_per_grid>" << std::endl
		<< "     The number of blocks per grid" << std::endl
		<< "  -i <num_iterations>|--iterations <num_iterations>" << std::endl
		<< "     The number of iterations to launch the kernel" << std::endl
		<< "  -stride <stride>" << std::endl
		<< "     Stride parameter for global-stride test. Not that size parameter is ignored then." << std::endl
		<< "  -offset <offset>" << std::endl
		<< "     Offset parameter for global-offset test. Not that size parameter is ignored then." << std::endl
		<< "" << std::endl;
}
