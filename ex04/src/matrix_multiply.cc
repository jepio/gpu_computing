#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <stdio.h>
#include <chCommandLine.h>
#include <chTimer.hpp>

const static int DEFAULT_BENCHMARK 		 = 0;
const static int DEFAULT_MATRIX_SIZE 	 = 5;
const static int DEFAULT_MIN_MATRIX_SIZE = 0;
const static int DEFAULT_MAX_MATRIX_SIZE = 1e4;
const static int DEFAULT_STEPS			 = 20;
const static int DEFAULT_ITERATIONS		 = 1E2;

// Function Prototype for printHelp
void printHelp(char *);

// Data type for storing a two-dimensional matrix
struct IntMatrix2D {
    unsigned rowCnt;  // number of rows
    unsigned colCnt;  // number of columns
    int** elements;	  // row-wise data storage, i.e. first index of two-dimensional
    // array is row and the second column
};

/*
 * allocIntMatrix2D:
 *   allocates memory for the given matrix according to its dimensions.
 */
void allocIntMatrix2D(IntMatrix2D& m) {
    m.elements = (int**)malloc(m.rowCnt * sizeof(int*));
    for (int i = 0; i < m.rowCnt; ++i) {
        m.elements[i] = (int*)malloc(m.colCnt * sizeof(int));
    }
}

/*
 * freeIntMatrix2D:
 *   releases the memory for the given matrix.
 */
void freeIntMatrix2D(IntMatrix2D& m) {
    for (int i = 0; i < m.rowCnt; ++i) {
        free(m.elements[i]);
    }
    free(m.elements);
}

/*
 * initMatricies:
 *   Initializes the two given matrices according to the rule given on the
 *   exercise sheet...
 */
void initMatricies(IntMatrix2D& a, IntMatrix2D& b) {
    if (a.rowCnt != b.rowCnt || a.colCnt != b.colCnt) {
        std::cout << "***" << std::cout
            << "*** Error - The dimensions of matrix A and B must match!" << std::endl
            << "***" << std::endl;
        exit(-1);
    }
    for (unsigned int i = 0; i < a.rowCnt; ++i) {
        for (unsigned int j = 0; j < a.colCnt; ++j) {
            a.elements[i][j] = i+j;
            b.elements[i][j] = i*j;
        }
    }
}

/*
 * multiplyInt2DMatrices:
 *   multiplies the given matrices a and b and stores the result in the given
 *   matrix c, assuming that its memory is already allocated correctly.
 */
void multiplyInt2DMatrices(const IntMatrix2D& a, const IntMatrix2D& b, IntMatrix2D& c) {
    if (a.colCnt != b.rowCnt) {
        std::cout << "***" << std::cout
            << "*** Error - The number of columns in matrix A need "
            << "to match the number of columns in matrix B!" << std::endl
            << "***" << std::endl;
        exit(-1);
    }
    if (c.colCnt != b.colCnt || c.rowCnt != a.rowCnt) {
        std::cout << "***" << std::cout
            << "*** Error - Matrix C is not correctly shaped!" << std::endl
            << "***" << std::endl;
        exit(-1);
    }

    // perform multiplication
    for (unsigned int i = 0; i < a.rowCnt; ++i) {
        for (unsigned int j = 0; j < b.colCnt; ++j) {
            int sum = 0;
            for (unsigned int k = 0; k < a.rowCnt; ++k) {
                sum += a.elements[i][k] * b.elements[k][j];
            }
            c.elements[i][j] = sum;
        }
    }
}

/*
 * printIntMatrix2D:
 *   prints the given matrix to std::cout
 */
void printIntMatrix2D(const IntMatrix2D& m) {
    std::cout << std::endl;
    for (unsigned int i = 0; i < m.rowCnt; ++i) {
        for (unsigned int j = 0; j <  m.colCnt; ++j) {

            std::cout << std::setw(4) << m.elements[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

/*
 * Main:
 *   Test- and benchmark program to evaluate matrix-multiplication in CPU.
 */
int main (int argc, char * argv[])
{
    // Show Help
    bool optShowHelp = chCommandLineGetBool("h", argc, argv);
    if (!optShowHelp)
        optShowHelp = chCommandLineGetBool("help", argc, argv);
    if (optShowHelp) {
        printHelp(argv[0]);
        exit(0);
    }

    // Initialize configuration variables
    int optBenchmark = 0;
    int optMatrixSize = 0;
    int optMinMatrixSize = 0;
    int optMaxMatrixSize = 0;
    int optSteps = 0;
    int optNumIterations = 0;

    // Get program configuration parameters
    chCommandLineGet<int> (&optBenchmark,"b", argc, argv);
    optBenchmark = (optBenchmark != 0) ? optBenchmark : DEFAULT_BENCHMARK;

    chCommandLineGet<int> (&optMatrixSize,"n", argc, argv);
    optMatrixSize = (optMatrixSize != 0) ? optMatrixSize : DEFAULT_MATRIX_SIZE;

    chCommandLineGet<int> (&optMinMatrixSize,"min_n", argc, argv);
    optMinMatrixSize = (optMinMatrixSize != 0) ? optMinMatrixSize : DEFAULT_MIN_MATRIX_SIZE;

    chCommandLineGet<int> (&optMaxMatrixSize,"max_n", argc, argv);
    optMaxMatrixSize = (optMaxMatrixSize != 0) ? optMaxMatrixSize : DEFAULT_MAX_MATRIX_SIZE;

    chCommandLineGet<int> (&optSteps,"s", argc, argv);
    optSteps = (optSteps != 0) ? optSteps : DEFAULT_STEPS;

    chCommandLineGet<int> (&optNumIterations,"i", argc, argv);
    optNumIterations = (optNumIterations != 0) ? optNumIterations : DEFAULT_ITERATIONS;

    // If no benchmarking should be performed, set min- and max-sizes equally to given matrix size
    if (optBenchmark != 1) {
        optMinMatrixSize = optMatrixSize;
        optMaxMatrixSize = optMatrixSize;
        optSteps = 1;
    }

    // Perform benchmarking for different matrix sizes
    // (or loop only once in case no benchmarking is wanted)
    for (int n = optMinMatrixSize; n <= optMaxMatrixSize;
            n += (optMaxMatrixSize - optMinMatrixSize + 1) / optSteps) {
        // Initialize matrix a, b and c according to matrix-size
        IntMatrix2D a; a.rowCnt = n; a.colCnt = n;
        IntMatrix2D b; b.rowCnt = n; b.colCnt = n;
        IntMatrix2D c; c.rowCnt = n; c.colCnt = n;
        allocIntMatrix2D(a); allocIntMatrix2D(b); allocIntMatrix2D(c);
        initMatricies(a,b);

        // No timing, if no benchmarking wanted
        if (optBenchmark != 1) {
            printIntMatrix2D(a);
            printIntMatrix2D(b);
            multiplyInt2DMatrices(a,b,c);
            printIntMatrix2D(c);
            // Perform multiplication multiple times and average measured CPU-time in
            // case benchmarking is wanted
        } else {
            ChTimer timer;
            timer.start();
            for (int i = 1; i <= optNumIterations; ++i) {
                multiplyInt2DMatrices(a,b,c);
            }
            timer.stop();
            // The number of FLOPs is determined by the number of operands used in the
            // matrix-multiplication. The algorithm uses three nested loops from 1 to n,
            // with the inner-most loop performing 2 operations (1 multiplication and 1 addition).
            // Thus, theoretically 2*n³ FLOPs are performed. However, considering the used
            // CPU (i7) the number of operations per iteration should be only a single one since
            // Multiply-Add operations can be performed atomically hardware-wise.
            std::cout << n << ";" << std::fixed
                << std::setw(6) << n*n*n / timer.getTime(optNumIterations) / (1E09)
                << " GFLOP/s" << std::endl;
        }

        // Clean up
        freeIntMatrix2D(a);
        freeIntMatrix2D(b);
        freeIntMatrix2D(c);
    }
}

void printHelp(char * programName)
{
    std::cout
        << "Usage: " << std::endl
        << "  " << programName << "[-n <matrix_size>]" << std::endl
        << "  -n <matrix_size>" << std::endl
        << "     The number of elements per matrix dimension in a two-dimensional " << std::endl
        << "     matrix, in case benchmarking is deactivated" << std::endl
        << "  -min_n <matrix_size>" << std::endl
        << "     The smallest matrix size in case of activated benchmarking" << std::endl
        << "  -max_n <matrix_size>" << std::endl
        << "     The largest matrix size in case of activated benchmarking" << std::endl
        << "  -s <steps>" << std::endl
        << "     The number of matrix-size increases to perform in case of activated benchmarking" << std::endl
        << "  -b <benchmarking>" << std::endl
        << "     Flag whether to activate benchmarking or not. Set to 1 to activate." << std::endl
        << "  -i <num_iterations>" << std::endl
        << "     The number of iterations to perform the matrix-multiplication in case of activated benchmarking" << std::endl
        << "" << std::endl;
}
