from read import *

#blocks = [1, 2, 3, 4] + range(5, 101, 5)
blocks = range(1, 200, 2)
sizes = [49152 * b for b in blocks]
fill_index_cols(sizes)
fill_cols([1024])
read_file("blocks_g2s.out")
json2csv(data)

