import traceback
import sys
import json
import csv

data = {}

def fill_index_cols(rng):
    for i in rng:
        data[i] = {}

def fill_cols(rng):
    for i in data:
        for j in rng:
            data[i][j] = None

def extract_num(fields, pos):
    return fields[pos].split("=")[1]

def extract_info(line):
    line = line.strip()
    fields = line.split(",")
    size = int(extract_num(fields, 1))
    blocks = int(extract_num(fields, 3))
    bw = float(extract_num(fields, 4)[:-4])
    return (size, blocks, bw)

def read_file(name):
    with open(name) as file_:

        lines = file_.readlines()[4::5]
        fields = (extract_info(line) for line in lines)
        for s, bl, bw in fields:
            data[s][bl] = bw

def json2csv(json_data):
    output = csv.writer(sys.stdout)
    sorted_keys = sorted(json_data.keys())
    first = sorted_keys[0]
    output.writerow([None] + sorted(json_data[first].keys()))
    for row in sorted_keys:
        output.writerow([row] + [json_data[row][k] for k in 
                                 sorted(json_data[row].keys())])

if __name__ == "__main__":
    try:
        fill_index_cols([1024 * m for m in xrange(1, 48, 5)]+[48*1024])
        fill_cols([32 * t for t in [1,2,4,6,8,12,16,20,24,32]])
        name = sys.argv[1]
        read_file(name)
        # print json.dumps(data, sort_keys=True, indent=4)
        json2csv(data)
    except Exception as e:
        print e
        print traceback.format_exc()
