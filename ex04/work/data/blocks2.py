from read import *

blocks = range(1, 200, 2)
sizes = [49152 * b for b in blocks]

fill_index_cols(sizes)
fill_cols([1024])
read_file("blocks_s2g.out")
json2csv(data)
