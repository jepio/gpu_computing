import re

expr = r".*size=\s*(\d+).*gDim=\s*(\d+).*bDim=\s*(\d+).*bw=\s*(.*)GB/s"
ptrn = re.compile(expr)

data = open("s2r.csv").readlines()

data = (re.match(ptrn, l).groups() for l in data)

data = (" ".join(l) for l in data)

file_ = open("s2r_2.csv","w")
for l in data:
    file_.write(l)
    file_.write("\n")

file_.close()

