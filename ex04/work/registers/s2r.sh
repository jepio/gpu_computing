#!/usr/bin/env bash
#SBATCH --gres=gpu
#SBATCH -o s2r.out

LOC=../../bin/

start_m=1024
steps_m="1 2 4 6 8 12 16 20 24 48"

start_t=32
steps_t="1 2 4 6 8 12 16 20 24 32"

start_b=1
steps_b="1 5 10 50 100 500 1000 5000 10000"

for m in $steps_m
do
    for b in $steps_b
    do
        for t in $steps_t
        do
            blocks=$((start_b * b))
            threads=$((start_t * t))
            mems=$((start_m * m))
            $LOC/memCpy --global2shared -s $mems -g $blocks -t $threads -i 1000
        done
    done
done
