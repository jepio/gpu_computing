#!/usr/bin/env bash

files="g2s.out s2g.out"

for i in $files
do
    base=$(basename $i .out)
    python read.py $i > ${base}.csv
done

