#!/usr/bin/env bash
#SBATCH --gres=gpu
#SBATCH -o blocks_s2g.out

LOC=../bin/

#blocks="1 2 3 4 "
blocks+=$(seq 1 2 200)

for b in $blocks
do
    $LOC/memCpy --shared2global -s 49152 -g $b -t 1024 -i 10000
done
