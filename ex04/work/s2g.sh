#!/usr/bin/env bash
#SBATCH --gres=gpu
#SBATCH -o s2g.out

LOC=../bin/

start_m=1024
steps_m=$(seq 1 5 48)
steps_m+=" 48"

start_t=32
steps_t="1 2 4 6 8 12 16 20 24 32"

for m in $steps_m
do
    for t in $steps_t
    do
        threads=$((start_t * t))
        mems=$((start_m * m))
        $LOC/memCpy --shared2global -s $mems -g 1 -t $threads -i 10000
    done
done
