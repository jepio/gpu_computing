#include <cstdlib>
#include <cstdio>
#include <cmath>

__global__ void reduce(const float *in, float *out, size_t arr_size)
{
    extern __shared__ float s_in[];

    int idx = threadIdx.x + blockDim.x * blockIdx.x;
    int tid = threadIdx.x;
    int num_threads = gridDim.x * blockDim.x;

    // Initialize shared memory to not have to check extra conditions later
    s_in[tid] = 0.0f;

    if (idx < arr_size) {
        // Every thread reads its own element
        s_in[tid] = in[idx];

        // Every thread reduces extra elements for arr_size > num_threads
        int i = idx + num_threads;
        while (i < arr_size) {
            s_in[tid] += in[i];
            i += num_threads;
        }

        // Now the elements inside shared memory will be reduced
        __syncthreads();
        int max = log2f(blockDim.x - 0.5f) + 1;
        max = 1 << max;

        for (int i = max / 2; i > 0; i >>= 1) {
            if (tid < i && tid + i < blockDim.x) {
                s_in[tid] += s_in[tid + i];
            }
            __syncthreads();
        }
    }

    if (tid == 0) {
        out[blockIdx.x] = s_in[0];
    }
}

int main(int argc, char **argv)
{

    const int n = std::atoi(argv[1]);
    float *a = new float[n];

    for (int i = 0; i < n; ++i)
        a[i] = i;


    dim3 threads = 32;
    dim3 blocks = (n + threads.x - 1) / threads.x;

    blocks.x = min(blocks.x, 1024);

    float *d_a;
    float *d_b;
    float *d_final;
    cudaMalloc(&d_a, n * sizeof(*d_a));
    cudaMalloc(&d_b, blocks.x * sizeof(*d_b));
    cudaMalloc(&d_final, sizeof(*d_final));
    cudaMemcpy(d_a, a, n * sizeof(*a), cudaMemcpyHostToDevice);

    reduce<<<blocks, threads, sizeof(*a) * threads.x>>>(d_a, d_b, n);
    reduce<<<1, blocks, sizeof(*a) * blocks.x>>>(d_b, d_final, blocks.x);

    float result[1];
    cudaMemcpy(result, d_final,sizeof(*d_final), cudaMemcpyDeviceToHost);
    cudaFree(d_b);
    cudaFree(d_a);
    cudaFree(d_final);
    printf("Result is: %f\n", result[0]);
}
