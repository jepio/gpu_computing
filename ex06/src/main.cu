/**************************************************************************************************
 *
 *       Computer Engineering Group, Heidelberg University - GPU Computing
 *Exercise 06
 *
 *                 Gruppe : TODO
 *
 *                   File : main.cu
 *
 *                Purpose : Reduction
 *
 **************************************************************************************************/

#include <cmath>
#include <iostream>
#include <cassert>
#include <cstdlib>
#include <cstdio>
#include "../inc/chCommandLine.h"
#include "../inc/chTimer.hpp"

const static int DEFAULT_MATRIX_SIZE = 1024;
const static int DEFAULT_BLOCK_DIM = 128;

//
// Function Prototypes
//
void printHelp(char *);

// CPU reduction, unoptimized

float cpu_reduction(float *inputArray, int numElements)
{
    float sum = 0;

    for (int i = 0; i < numElements; i++) {
        sum += inputArray[i];
    }
    return sum;
}

bool ResultsEqual(float gpuRes, float cpuRes)
{
    if (fabs((gpuRes - cpuRes) / (gpuRes + cpuRes)) > 1E-04) {
        return false;
    }

    return true;
}

__global__ void bad_reduce(int n, const float *in, float *out)
{
    __shared__ float s_in[1024];

    int idx = threadIdx.x + blockDim.x * blockIdx.x;
    int total = gridDim.x * blockDim.x;
    int tid = threadIdx.x;

    s_in[tid] = 0.0f;

    float sum = 0.0f;
    while (idx < n) {
        sum += in[idx];
        idx += total;
    }
    s_in[tid] = sum;

    __syncthreads();

    for (int i=1; i < blockDim.x; i *= 2) {
        if (tid % (2 * i) == 0 && tid + i < blockDim.x) {
            s_in[tid] += s_in[tid + i];
        }
        __syncthreads();
    }

    if (tid == 0)
        out[blockIdx.x] = s_in[0];
}

//
// Reduction_Kernel
//
template <int BlockSize>
__device__ void warp_reduce(volatile float *s, int tid)
{
    if (BlockSize >= 64) s[tid] += s[tid + 32];
    if (BlockSize >= 32) s[tid] += s[tid + 16];
    if (BlockSize >= 16) s[tid] += s[tid +  8];
    if (BlockSize >=  8) s[tid] += s[tid +  4];
    if (BlockSize >=  4) s[tid] += s[tid +  2];
    if (BlockSize >=  2) s[tid] += s[tid +  1];
}

template <int BlockSize>
__global__ void optimized_kernel(int arr_size, const float *in, float *out)
{
    __shared__ float s_in[1024];

    int idx = threadIdx.x + 2 * blockDim.x * blockIdx.x;
    int tid = threadIdx.x;
    int num_threads = 2 * gridDim.x * blockDim.x;

    // Initialize shared memory to not have to check extra conditions later
    s_in[tid] = 0.0f;

    // Every thread reads its own element
    
    float sum = 0.0f; 

    if (idx < arr_size) {
        while (idx < arr_size - BlockSize) {
            sum += in[idx] + in[idx + BlockSize];
            idx += num_threads;
        }
        if (idx < BlockSize)
            sum += in[idx];

        s_in[tid] = sum;

        __syncthreads();

       
        for (int i = BlockSize / 2; i > 0; i >>= 1) {
            if (tid < i)
                s_in[tid] += s_in[tid + i];
            __syncthreads();
        }

    }
    if (tid == 0) {
        out[blockIdx.x] = s_in[0];
    }
}

void run_optimized(dim3 blocks, dim3 threads, int arrsize, const float *in, float *out)
{
    int bdim = threads.x;
    switch (bdim) {
    case 1024:
        optimized_kernel<1024><<<blocks, threads>>>(arrsize, in, out);
        break;
    case 512:
        optimized_kernel<512><<<blocks, threads>>>(arrsize, in, out);
        break;
    case 256:
        optimized_kernel<256><<<blocks, threads>>>(arrsize, in, out);
        break;
    case 128:
        optimized_kernel<128><<<blocks, threads>>>(arrsize, in, out);
        break;
    case 64:
        optimized_kernel<64><<<blocks, threads>>>(arrsize, in, out);
        break;
    case 32:
        optimized_kernel<32><<<blocks, threads>>>(arrsize, in, out);
        break;
    case 16: 
        optimized_kernel<16><<<blocks, threads>>>(arrsize, in, out);
        break;
    case 8:
        optimized_kernel<8><<<blocks, threads>>>(arrsize, in, out);
        break;
    case 4:
        optimized_kernel<4><<<blocks, threads>>>(arrsize, in, out);
        break;
    case 2:
        optimized_kernel<2><<<blocks, threads>>>(arrsize, in, out);
        break;
    case 1:
        optimized_kernel<1><<<blocks, threads>>>(arrsize, in, out);
        break;
    default:
        printf("Bdim: %d \n", bdim);
        assert(bdim == 0);
    }
}
//
// Main
//
int main(int argc, char *argv[])
{
    bool showHelp = chCommandLineGetBool("h", argc, argv);
    if (!showHelp) {
        showHelp = chCommandLineGetBool("help", argc, argv);
    }

    if (showHelp) {
        printHelp(argv[0]);
        exit(0);
    }

    std::cout << "***" << std::endl << "*** Starting ..." << std::endl << "***"
              << std::endl;

    ChTimer memCpyH2DTimer, memCpyD2HTimer;
    ChTimer kernelTimer;

    ChTimer cpuTimer;

    //
    // Allocate Memory
    //
    int numElements = 0;
    chCommandLineGet<int>(&numElements, "s", argc, argv);
    chCommandLineGet<int>(&numElements, "size", argc, argv);
    numElements = numElements != 0 ? numElements : DEFAULT_MATRIX_SIZE;
    //
    // Host Memory
    //
    bool pinnedMemory = chCommandLineGetBool("p", argc, argv);
    if (!pinnedMemory) {
        pinnedMemory = chCommandLineGetBool("pinned-memory", argc, argv);
    }

    float *h_dataIn = NULL;
    float *h_dataOut = NULL;
    if (!pinnedMemory) {
        // Pageable
        h_dataIn = static_cast<float *>(
            malloc(static_cast<size_t>(numElements * sizeof(*h_dataIn))));

        h_dataOut = static_cast<float *>(
            malloc(static_cast<size_t>(sizeof(*h_dataOut))));
    } else {
        // Pinned
        cudaMallocHost(&h_dataIn,
                       static_cast<size_t>(numElements * sizeof(*h_dataIn)));

        cudaMallocHost(&h_dataOut, static_cast<size_t>(sizeof(*h_dataOut)));
    }

    // Initialise host memory
    for (int i = 0; i < numElements; ++i)
        h_dataIn[i] = i;

    // Device Memory
    float *d_dataIn = NULL;
    float *d_dataOut = NULL;
    cudaError_t err = cudaMalloc(
        &d_dataIn, static_cast<size_t>(numElements * sizeof(*d_dataIn)));

    if (err != cudaSuccess) {
        printf("The error is %s", cudaGetErrorString(err));
    }

    cudaMalloc(&d_dataOut, static_cast<size_t>(1024 * sizeof(*d_dataOut)));

    if (h_dataIn == NULL || h_dataOut == NULL || d_dataIn == NULL ||
        d_dataOut == NULL) {
        std::cout << "\033[31m***" << std::endl
                  << "*** Error - Memory allocation failed" << std::endl
                  << "***\033[0m" << std::endl;
        if (h_dataIn == NULL)
            std::cout << "h_dataIn fucked up" << std::endl;
        if (h_dataOut == NULL)
            std::cout << "h_dataOut fucked up" << std::endl;
        if (d_dataIn == NULL)
            std::cout << "d_dataIn fucked up" << std::endl;
        if (d_dataOut == NULL)
            std::cout << "d_dataOut fucked up" << std::endl;
        exit(-1);
    }

    // Init h_dataOut
    *h_dataOut = 0.0;

    //
    // Copy Data to the Device
    //
    memCpyH2DTimer.start();

    cudaMemcpy(d_dataIn, h_dataIn,
               static_cast<size_t>(numElements * sizeof(*d_dataIn)),
               cudaMemcpyHostToDevice);
    cudaMemcpy(d_dataOut, h_dataOut, static_cast<size_t>(sizeof(*d_dataOut)),
               cudaMemcpyHostToDevice);

    memCpyH2DTimer.stop();

    //
    // Get Kernel Launch Parameters
    //
    int blockSize = 0, gridSize = 0;

    // Block Dimension / Threads per Block
    chCommandLineGet<int>(&blockSize, "t", argc, argv);
    chCommandLineGet<int>(&blockSize, "threads-per-block", argc, argv);
    blockSize = blockSize != 0 ? blockSize : DEFAULT_BLOCK_DIM;

    if (blockSize > 1024) {
        std::cout << "\033[31m***" << std::endl
                  << "*** Error - The number of threads per block is too big"
                  << std::endl << "***\033[0m" << std::endl;

        exit(-1);
    }

    gridSize =
        ceil(static_cast<float>(numElements) / static_cast<float>(blockSize));
    gridSize = min(gridSize, 1024);

    dim3 grid_dim = dim3(gridSize);
    dim3 block_dim = dim3(blockSize);

    kernelTimer.start();
    
    /*
    run_optimized(grid_dim, block_dim, numElements, d_dataIn, d_dataOut);
    grid_dim = 1 << static_cast<int>(log2f(gridSize - 0.5f) + 1) - 1;
    run_optimized(1, grid_dim, gridSize, d_dataOut, d_dataOut);
    */
    bad_reduce<<<grid_dim, block_dim>>>
        (numElements, d_dataIn, d_dataOut);
    bad_reduce<<<1, grid_dim>>>
        (gridSize, d_dataOut, d_dataOut);
    // Synchronize
    cudaDeviceSynchronize();

    kernelTimer.stop();
    // Check for Errors
    cudaError_t cudaError = cudaGetLastError();
    if (cudaError != cudaSuccess) {
        std::cout << "\033[31m***" << std::endl << "***ERROR*** " << cudaError
                  << " - " << cudaGetErrorString(cudaError) << std::endl
                  << "***\033[0m" << std::endl;

        return -1;
    }


    //
    // Copy Back Data
    //
    memCpyD2HTimer.start();

    cudaMemcpy(h_dataOut, d_dataOut, static_cast<size_t>(sizeof(*d_dataOut)),
               cudaMemcpyDeviceToHost);

    memCpyD2HTimer.stop();

    float cpuResult = 0;
    // check result
    cpuTimer.start();

    cpuResult = cpu_reduction(h_dataIn, numElements);

    cpuTimer.stop();

    if (!ResultsEqual(*h_dataOut, cpuResult)) {
        std::cout << "\033[31m***" << "NO MATCH" << "***\033[0m"<< std::endl;
        std::cout << "CPU:" << cpuResult << " GPU:" << *h_dataOut << std::endl;
    } else {
        std::cout << "MATCH" << std::endl;
        std::cout << "CPU:" << cpuResult << std::endl;
    }

    // Free Memory
    if (!pinnedMemory) {
        free(h_dataIn);
        free(h_dataOut);
    } else {
        cudaFreeHost(h_dataIn);
        cudaFreeHost(h_dataOut);
    }
    cudaFree(d_dataIn);
    cudaFree(d_dataOut);

    // Print Meassurement Results
    std::cout
        << "***" << std::endl << "*** Results:" << std::endl
        << "***    Num Elements: " << numElements << std::endl
        << "***    Time to Copy to Device: " << 1e3 * memCpyH2DTimer.getTime()
        << " ms" << std::endl << "***    Copy Bandwidth: "
        << 1e-9 * memCpyH2DTimer.getBandwidth(numElements * sizeof(*h_dataIn))
        << " GB/s" << std::endl
        << "***    Time to Copy from Device: " << 1e3 * memCpyD2HTimer.getTime()
        << " ms" << std::endl << "***    Copy Bandwidth: "
        << 1e-9 * memCpyD2HTimer.getBandwidth(sizeof(*h_dataOut)) << " GB/s"
        << std::endl
        << "***    Time for Reduction: " << 1e3 * kernelTimer.getTime() << " ms"
        << std::endl
        << "***    Time for Reduction on CPU: " << 1e3 * cpuTimer.getTime()
        << " ms" << std::endl << "***" << std::endl
        << "***    Bandwidth for GPU: " << 1e-9 * kernelTimer.getBandwidth(
                numElements * sizeof(*h_dataIn)) << " GB/s" << std::endl;
    return 0;
}

void printHelp(char *argv)
{
    std::cout
        << "Help:" << std::endl << "  Usage: " << std::endl << "  " << argv
        << " [-p] [-s <num-elements>] [-t <threads_per_block>]" << std::endl
        << "" << std::endl << "  -p|--pinned-memory" << std::endl
        << "	Use pinned Memory instead of pageable memory" << std::endl << ""
        << std::endl << "  -s <num-elements>|--size <num-elements>" << std::endl
        << "	The size of the Matrix" << std::endl << "" << std::endl
        << "  -t <threads_per_block>|--threads-per-block <threads_per_block>"
        << std::endl << "	The number of threads per block" << std::endl << ""
        << std::endl;
}
