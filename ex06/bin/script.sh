#!/usr/bin/env bash
#SBATCH --gres=gpu
#SBATCH -o bad-kernel

threads="32 64 128 192 256 512 1024"

elems=$(seq 10 2 26)

for t in $threads
do
    for e in $elems
    do
        total=$((2<<e))
        echo "Threads: $t"
        ./reduction -p -s $total -t $t
    done
done
