import re
import sys

threads = re.compile(r"Threads: (\d+)")
elems = re.compile(r"Num Elements: (\d+)")
time = re.compile(r"Time for Reduction: (\d+\.\d+) ms")
bw = re.compile(r"Bandwidth for GPU: (\d+\.\d+) GB/s")

filename = sys.argv[1]

fh = open(filename)
lines = fh.read()

threads = [int(l.group(1)) for l in re.finditer(threads, lines)]
elems = [int(l.group(1)) for l in re.finditer(elems, lines)]
time = [l.group(1) for l in re.finditer(time, lines)]
bw = [float(l.group(1)) for l in re.finditer(bw, lines)]

fh.close()
#print threads
#print elems
#print bw


data =  zip(threads, zip(elems, bw))

main_dict = {}

for t, (e, b) in data:
    if t not in main_dict:
        main_dict[t] = {}
    main_dict[t][e] = b

import csv
with open("bad_kernel.csv", "w") as fh:
    f = csv.writer(sys.stderr)
    keys = sorted(main_dict.keys())
    index = sorted(main_dict[keys[0]].keys())
    f.writerow([None] + index)
    for i in keys:
        f.writerow([i] + [main_dict[i][k] for k in index])



#print main_dict

